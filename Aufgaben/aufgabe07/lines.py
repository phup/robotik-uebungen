#!/usr/bin/env python
import roslib
# roslib.load_manifest('my_package')
import sys
import rospy
import cv2
import numpy as np
from std_msgs.msg import String
from geometry_msgs.msg import Pose
from geometry_msgs.msg import Point
from geometry_msgs.msg import Quaternion
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
from operator import itemgetter
from std_msgs.msg import Float32
from nav_msgs.msg import Odometry
from math import sqrt
from std_msgs.msg import UInt8
from std_msgs.msg import Int16
#import matplotlib
#matplotlib.use('Agg')
from matplotlib import pyplot as plt

# from __future__ import print_function
def threshold_line(img):
    ret, thresh = cv2.threshold(img, 190, 255, cv2.THRESH_BINARY)
    return thresh

def hough_lines(bgr_image):
    gray_image = cv2.cvtColor(bgr_image, cv2.COLOR_BGR2GRAY)
    binary_img = threshold_line(gray_image)
    edges = cv2.Canny(binary_img, 50, 150, apertureSize=3)
    """
    cv2.imshow("edges",edges)
    cv2.waitKey(0)
    """
    

    lines = cv2.HoughLines(edges, 1, np.pi / 90, 100)
    """
    for line in lines:
         for rho, theta in line:
             a = np.cos(theta)
             b = np.sin(theta)
             x0 = a * rho
             y0 = b * rho
             x1 = int(x0 + 1000 * (-b))
             y1 = int(y0 + 1000 * (a))
             x2 = int(x0 - 1000 * (-b))
             y2 = int(y0 - 1000 * (a))
    
             cv2.line(bgr_image, (x1, y1), (x2, y2), (0, 0, 255), 2)
    
    cv2.imshow('houghlines3.jpg', bgr_image)
    cv2.waitKey(0)
    
    """

    return lines

def group_lines(lines,bgr_image,  number_of_lines = 2):

    new_lines = []

    result_lines = []


    for line in lines:
        new_lines.append(line[0])
    new_lines  = np.array(new_lines)
    sorted_new_lines = new_lines[new_lines[:,1].argsort()]



    current_rho, current_theta = sorted_new_lines[0]
    sorted_new_lines = np.delete(sorted_new_lines, (0), axis=0)



    votes = 1
    for line in sorted_new_lines:
        rho, theta = line
        if abs(theta-current_theta) < np.pi/18 and abs(rho-current_rho) <= 20:
            current_theta = (theta + current_theta) / 2
            current_rho = (rho + current_rho) / 2
            votes +=1
        elif votes >=2:
            votes = 1
            result_lines.append([current_rho,current_theta])
            current_rho = rho
            current_theta = theta
        else:
            current_rho = rho
            current_theta = theta
    if votes >= 2:
        result_lines.append([current_rho, current_theta])

    print(result_lines)
    if result_lines == []:
      result_lines = []
    else:
      result_lines = [result_lines[0]]
    

    
    
    for rho, theta in result_lines:
        a = np.cos(theta)
        b = np.sin(theta)
        x0 = a * rho
        y0 = b * rho
        x1 = int(x0 + 1000 * (-b))
        y1 = int(y0 + 1000 * (a))
        x2 = int(x0 - 1000 * (-b))
        y2 = int(y0 - 1000 * (a))

        cv2.line(bgr_image, (x1, y1), (x2, y2), (0, 0, 255), 2)

    """
    cv2.imshow('houghlines3.jpg', bgr_image)
    cv2.waitKey(0)
    """
    
    
    

    
    return (result_lines, bgr_image)
    


class image_converter:

  def __init__(self):
    self.image_pub = rospy.Publisher("/image_processing/bin_img",Image, queue_size=100)
    self.bridge = CvBridge()
    self.image_sub = rospy.Subscriber("/camera/color/image_raw",Image,self.image_callback, queue_size=100)
    self.error_array = []
    self.change_counter = 0


  def image_callback(self,data):
    try:
      cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
      cv_image_height = len(cv_image)
      cv_image_width_middle = len(cv_image[0]) / 2
      optimal_angle = 0
    except CvBridgeError as e:
      print(e)

    current_angle = 86
    gray=cv2.cvtColor(cv_image, cv2.COLOR_BGR2GRAY)
    hough_lines_result = hough_lines(cv_image)
    grouped_lines, grouped_lines_img = group_lines(hough_lines_result, cv_image)

    
    if len(grouped_lines) == 1:
      rho, theta = grouped_lines[0]
      # line_equation = (-np.cos(theta)/np.sin(theta))*x + (rho/np.sin(theta))
      line_equation_x_0 = ( -1* (rho/np.sin(theta)))/(-np.cos(theta)/np.sin(theta)) 
      error = line_equation_x_0 - cv_image_width_middle
      steering_angle_change = 0.1*(error)
      print(steering_angle_change)
      self.error_array.append(steering_angle_change)
      self.change_counter += 1
      if self.change_counter == 10:
        print(self.error_array)
        mean_error = np.mean(self.error_array)

        current_angle =  current_angle + mean_error
	pub_steering.publish(current_angle)
        self.change_counter = 0
        self.error_array = []
	
    try:
      """
      print("image_published")
      
      cv2.imshow("img", grouped_lines_img)
      cv2.waitKey(1)
      """
      
      self.image_pub.publish(self.bridge.cv2_to_imgmsg(grouped_lines_img, "bgr8"))
    except CvBridgeError as e:
      print(e)


def callbackOdom(msg):
    global last_odom
    last_odom = msg
def waitForFirstOdom():
    while not rospy.is_shutdown() and last_odom is None:
        rospy.loginfo(
            "%s: No initial odometry message received. Waiting for message...",
            rospy.get_caller_id())
        rospy.sleep(1.0)

def callbackDrivingControl(msg):
    last_driving_control_info = msg.data

def change_angle(distance, command, speed, angle):
    pub_stop_start.publish(1)
    rospy.sleep(1)
    pub_steering.publish(angle)
    pub_stop_start.publish(0)
    rospy.sleep(1)
    pub_speed.publish(speed)



rospy.init_node('image_converter', anonymous=True)

# create subscribers and publishers
#sub_odom = rospy.Subscriber("odom", Odometry, callbackOdom, queue_size=100)
# wait for first odometry message, till adverting subscription of commands
#
#waitForFirstOdom()
pub_stop_start = rospy.Publisher("manual_control/stop_start",Int16,queue_size=100)
pub_speed = rospy.Publisher("manual_control/speed", Int16, queue_size=100)
pub_steering = rospy.Publisher("steering",UInt8,queue_size=100)
pub_info = rospy.Publisher("simple_drive_control/info", String, queue_size=100)  
ic = image_converter() 

try:
  rospy.spin()
except KeyboardInterrupt:
  print("Shutting down")
cv2.destroyAllWindows()



