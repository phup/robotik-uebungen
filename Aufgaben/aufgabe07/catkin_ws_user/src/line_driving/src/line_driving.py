#!/usr/bin/env python
import roslib
# roslib.load_manifest('my_package')
import sys
import rospy
import cv2
import numpy as np
from std_msgs.msg import String
from geometry_msgs.msg import Pose
from geometry_msgs.msg import Point
from geometry_msgs.msg import Quaternion
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
from operator import itemgetter
from std_msgs.msg import Float32
from nav_msgs.msg import Odometry
from math import sqrt
from std_msgs.msg import UInt8
from std_msgs.msg import Int16
#import matplotlib
#matplotlib.use('Agg')
#from matplotlib import pyplot as plt
"""
# --- definitions ---
last_driving_control_info = ""
epsilon = 0.05   # allowed inaccuracy for distance calculation
angle_left = 0
angle_straight = 86
angle_right = 180
last_odom = None
is_active = False
distance = 0.2
"""

start_angle = 86
# from __future__ import print_function
def threshold_line(img):
    ret, thresh = cv2.threshold(img, 190, 255, cv2.THRESH_BINARY)
    return thresh

def hough_lines(bgr_image):
    gray_image = cv2.cvtColor(bgr_image, cv2.COLOR_BGR2GRAY)
    binary_img = threshold_line(gray_image)
    edges = cv2.Canny(binary_img, 50, 150, apertureSize=3)
    """
    cv2.imshow("edges",edges)
    cv2.waitKey(0)
    """
    img_middle_height = int(len(bgr_image)/2)
    for height in range(int(len(edges)/2)):
        for width in range(len(edges[0])):
	    edges[height][width] = 0
    #print(edges)
    # edges[:img_middle_height][:] = 0
    lines = cv2.HoughLines(edges, 1, np.pi / 90, 50)
    """
    for line in lines:
         for rho, theta in line:
             a = np.cos(theta)
             b = np.sin(theta)
             x0 = a * rho
             y0 = b * rho
             x1 = int(x0 + 1000 * (-b))
             y1 = int(y0 + 1000 * (a))
             x2 = int(x0 - 1000 * (-b))
             y2 = int(y0 - 1000 * (a))
    
             cv2.line(bgr_image, (x1, y1), (x2, y2), (0, 0, 255), 2)
     
    cv2.imshow('houghlines3.jpg', bgr_image)
    cv2.waitKey(0)
    
    """

    return lines

def group_lines(lines,bgr_image,  number_of_lines = 2):

    new_lines = []

    result_lines = []


    for line in lines:
        new_lines.append(line[0])
    new_lines  = np.array(new_lines)
    sorted_new_lines = new_lines[new_lines[:,1].argsort()]



    current_rho, current_theta = sorted_new_lines[0]
    sorted_new_lines = np.delete(sorted_new_lines, (0), axis=0)



    votes = 1
    for line in sorted_new_lines:
        rho, theta = line
        if abs(theta-current_theta) < np.pi/18 and abs(rho-current_rho) <= 20:
            current_theta = (theta + current_theta) / 2
            current_rho = (rho + current_rho) / 2
            votes +=1
        elif votes >=2:
            votes = 1
            result_lines.append([current_rho,current_theta])
            current_rho = rho
            current_theta = theta
        else:
            current_rho = rho
            current_theta = theta
    if votes >= 2:
        result_lines.append([current_rho, current_theta])
    print(result_lines)
    # print(result_lines)
    if result_lines == []:
      final_result_line = []
      pub_speed.publish(0)
    else:
      last_theta = min(abs(result_lines[0][1]), abs(np.pi-result_lines[0][1]))
      final_result_line = result_lines[0]
      for i in result_lines:
          if min( abs(i[1]),abs(np.pi - i[1])) < last_theta:
              final_result_line = i
              last_theta = final_result_line[1]
      final_result_line = [final_result_line]
      #print(result_lines)
      #print(final_result_line)
    

    
    
    for rho, theta in final_result_line:
        a = np.cos(theta)
        b = np.sin(theta)
        x0 = a * rho
        y0 = b * rho
        x1 = int(x0 + 1000 * (-b))
        y1 = int(y0 + 1000 * (a))
        x2 = int(x0 - 1000 * (-b))
        y2 = int(y0 - 1000 * (a))

        cv2.line(bgr_image, (x1, y1), (x2, y2), (0, 0, 255), 2)

    """
    cv2.imshow('houghlines3.jpg', bgr_image)
    cv2.waitKey(0)
    """
    
    
    

    
    return (final_result_line, bgr_image)
    


class image_converter:

  def __init__(self):
    self.image_pub = rospy.Publisher("/image_processing/bin_img",Image, queue_size=100)
    self.bridge = CvBridge()
    self.image_sub = rospy.Subscriber("/camera/color/image_raw",Image,self.image_callback, queue_size=100)
    self.error_array = []
    self.change_counter = 0
    self.previous_angle_change = 0
    self.all_angles = []
    


  def image_callback(self,data):
    try:
      cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
      cv_image_height_middle = int(len(cv_image) *0.75)
      cv_image_width_middle = int(len(cv_image[0]) / 2)
      gray_image = cv2.cvtColor(cv_image, cv2.COLOR_BGR2GRAY)
      binary_img = threshold_line(gray_image)
      edges = cv2.Canny(binary_img, 50, 150, apertureSize=3)

      #print(cv_image.shape)
      optimal_angle = 0
      pub_speed.publish(0)
    except CvBridgeError as e:
      print(e)

    current_angle = start_angle
    gray=cv2.cvtColor(cv_image, cv2.COLOR_BGR2GRAY)
    #hough_lines_result = hough_lines(cv_image)
    #grouped_lines, grouped_lines_img = group_lines(hough_lines_result, cv_image)
    last_min=9999
    for i in range(len(binary_img[cv_image_height_middle])):
        pixel=binary_img[cv_image_height_middle][i]
        if pixel>0:
	    if abs(i-cv_image_width_middle)<last_min:
                last_min=i
    #if len(grouped_lines) == 1:
    if last_min!=9999:
      #print(last_min)
      #rho, theta = grouped_lines[0]
      #print(cv_image_height_middle)
      #line_equation_height_middle = int(( cv_image_height_middle- (rho/np.sin(theta)))/(-np.cos(theta)/np.sin(theta)))
      #print(rho,theta)
      #line_equation_height_middle = int((cv_image_height_middle  - rho/np.sin(theta))/ -1*(np.cos(theta)/np.sin(theta)))
      #print(line_equation_height_middle)
      line_equation_height_middle=last_min
      cv2.circle(cv_image,(line_equation_height_middle,cv_image_height_middle),9,(255,0,255))
      error = line_equation_height_middle - cv_image_width_middle
      steering_angle_change = 0.5*(error)
      self.all_angles.append(steering_angle_change)
      print(self.all_angles)
      if steering_angle_change * self.previous_angle_change < 0:
         # current_angle= start_angle + steering_angle_change
         self.error_array = []
         #self.change_counter = 0
      else:
        #print(steering_angle_change)
        self.error_array.append(steering_angle_change)
        self.change_counter += 1
        if self.change_counter == 10:
          #print(self.error_array)
          mean_error = np.median(self.error_array)
	  #print(mean_error)
          current_angle =  start_angle + mean_error
	  if current_angle > 180:
	    current_angle = 180
	  if current_angle < 0:
	    current_angle = 0
          #print(current_angle)
          self.change_counter = 0
          self.error_array = []
          pub_steering.publish(current_angle)
	
    try:
      """
      print("image_published")
      
      cv2.imshow("img", grouped_lines_img)
      cv2.waitKey(1)
      """
      
      self.image_pub.publish(self.bridge.cv2_to_imgmsg(cv_image, "bgr8"))
    except CvBridgeError as e:
      print(e)


def callbackOdom(msg):
    global last_odom
    last_odom = msg
def waitForFirstOdom():
    while not rospy.is_shutdown() and last_odom is None:
        rospy.loginfo(
            "%s: No initial odometry message received. Waiting for message...",
            rospy.get_caller_id())
        rospy.sleep(1.0)

def callbackDrivingControl(msg):
    last_driving_control_info = msg.data

def change_angle(distance, command, speed, angle):
    pub_stop_start.publish(1)
    rospy.sleep(1)
    pub_steering.publish(angle)
    pub_stop_start.publish(0)
    rospy.sleep(1)
    pub_speed.publish(speed)



rospy.init_node('image_converter', anonymous=True)

# create subscribers and publishers
#sub_odom = rospy.Subscriber("odom", Odometry, callbackOdom, queue_size=100)
# wait for first odometry message, till adverting subscription of commands
#
#waitForFirstOdom()
pub_stop_start = rospy.Publisher("manual_control/stop_start",Int16,queue_size=100)
pub_speed = rospy.Publisher("manual_control/speed", Int16, queue_size=100)
pub_steering = rospy.Publisher("steering",UInt8,queue_size=100)
pub_info = rospy.Publisher("simple_drive_control/info", String, queue_size=100)  
ic = image_converter() 

try:
  rospy.spin()
except KeyboardInterrupt:
  print("Shutting down")
cv2.destroyAllWindows()



