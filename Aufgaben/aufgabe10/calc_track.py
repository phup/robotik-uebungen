import cv2
from matplotlib import pyplot as plt
import numpy as np
img = cv2.imread("images/map.bmp")




Mittelpunkt_K1 = (195, 215)
Mittelpunkt_K2 = (405, 215)


Radius_inner_circle = 135
Radius_outer_circle = 165
H_G1_L1 = 80
H_G2_L1 = 350

H_G1_L2 = 50               
H_G2_L2 = 380




X_MIN, X_MAX = 0, 600
Y_MIN, Y_MAX = 0, 430
linke_grenze, rechte_grenze, mittle_grenze = 185, 415, 215
def closest_point_to_a_line(punkt, h):
    """
    Die gegebene Gerade: y = h
    """
    x, y = punkt
    P_x = int(round(x))

    return (P_x, h)


def closest_point_to_a_halfcircle(punkt, mittelpunkt, r):
    """
    (M_x, M_y) ist der Mittelpunkt des Kreises, und r ist der Radius
    """
    x, y = punkt
    M_x, M_y = mittelpunkt

    d = np.sqrt((x - M_x) ** 2 + (y - M_y) ** 2)
    P_x = int(round(r * (float(x - M_x) / d) + M_x))
    P_y = int(round(r * (float(y - M_y) / d) + M_y))

    return (P_x, P_y)


def find_closest_point(punkt,lane, distance):
    x, y = punkt

    # Überprüfe punkt zunächst, ob er gültig ist
    if x < X_MIN or x > X_MAX:
        print("Ungültiges x!")
        return (-1, -1)

    if y < Y_MIN or y > Y_MAX:
        print("Ungültiges y!")
        return (-1, -1)

    # Berechne den zutreffenden Punkt je nach dem vier Bereichen
    if x < linke_grenze:
        if lane == 1:
            ansx, ansy = closest_point_to_a_halfcircle(punkt, Mittelpunkt_K1, Radius_inner_circle)
            a = np.arccos((Mittelpunkt_K1[1]-ansy)/Radius_inner_circle) - distance/Radius_inner_circle
            ansx = Mittelpunkt_K1[0] - np.sin(a)*Radius_inner_circle
            ansy = Mittelpunkt_K1[1] - np.cos(a) * Radius_inner_circle

            cv2.circle(img, (Mittelpunkt_K1[1], Mittelpunkt_K1[0]), Radius_inner_circle, (0, 255, 255))
        if lane == 2:
            ansx, ansy = closest_point_to_a_halfcircle(punkt, Mittelpunkt_K1, Radius_outer_circle)
            a = np.arccos((Mittelpunkt_K1[1]-ansy)/Radius_outer_circle) - distance/Radius_outer_circle
            ansx = Mittelpunkt_K1[0] - np.sin(a)*Radius_outer_circle
            ansy = Mittelpunkt_K1[1] - np.cos(a) * Radius_outer_circle

            cv2.circle(img, (Mittelpunkt_K2[1], Mittelpunkt_K1[0]), Radius_outer_circle, (0, 255, 255))
        return (int(ansx),int(ansy))
    elif x > rechte_grenze:
        if lane == 1:
            ansx, ansy = closest_point_to_a_halfcircle(punkt, Mittelpunkt_K2, Radius_inner_circle)
            a = np.arccos((Mittelpunkt_K2[1]-ansy)/Radius_inner_circle) + distance/Radius_inner_circle
            ansx = Mittelpunkt_K2[0] + np.sin(a)*Radius_inner_circle
            ansy = Mittelpunkt_K2[1] - np.cos(a) * Radius_inner_circle

            cv2.circle(img, (Mittelpunkt_K2[1], Mittelpunkt_K2[0]), Radius_inner_circle, (0, 255, 255))
        if lane == 2:
            ansx, ansy = closest_point_to_a_halfcircle(punkt, Mittelpunkt_K2, Radius_outer_circle)
            a = np.arccos((Mittelpunkt_K2[1]-ansy)/Radius_outer_circle) - distance/Radius_outer_circle
            ansx = Mittelpunkt_K2[0] + np.sin(a)*Radius_outer_circle
            ansy = Mittelpunkt_K2[1] - np.cos(a) * Radius_outer_circle


            cv2.circle(img, (Mittelpunkt_K2[1], Mittelpunkt_K2[0]), Radius_outer_circle, (0, 255, 255))
        return (int(ansx),int(ansy))
    elif y < mittle_grenze:
        if lane == 1:
            ans = closest_point_to_a_line(punkt, H_G1_L1)
            ans = (ans[0]+ distance, ans[1])
        if lane == 2:
            ans = closest_point_to_a_line(punkt, H_G1_L2)
            ans = (ans[0] + distance, ans[1])
        return ans
    else:
        if lane == 1:
            ans = closest_point_to_a_line(punkt, H_G2_L1)
            ans = (ans[0] + distance, ans[1])
        if lane == 2:
            ans = closest_point_to_a_line(punkt, H_G2_L2)
            ans = (ans[0] + distance, ans[1])

    return ans
def umrechne_punkt(punkt):
    """
    y muss noch umgerechnet werden, indem y = Y_MAX - y,
    bevor punkt gezeichnet wird
    """
    x, y = punkt
    return (x, Y_MAX - y)



pointx, pointy = 260,107
print(pointy)
closest_pointx,closest_pointy = (find_closest_point((pointx,pointy),1, 50))
print(closest_pointx,closest_pointy)
cv2.circle(img,(closest_pointy, closest_pointx), 3, (255,0,0))
cv2.circle(img,(pointy,pointx), 3,(0,255,0))

cv2.namedWindow('image',cv2.WINDOW_NORMAL)
cv2.resizeWindow('image', 1000,1000)
cv2.imshow("image",img)
cv2.waitKey(0)
