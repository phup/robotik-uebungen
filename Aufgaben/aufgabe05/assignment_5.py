# !/usr/bin/env python
# coding: utf8


"""
author: Myz
time: 05:36 21.11.2018
"""

import numpy as np
import cv2 as cv
import time
from matplotlib import pyplot as plt
from sklearn import linear_model, datasets

THRESHOLD = 210

image_raw = "/home/phu/Schreibtisch/catkin_ws_user/assignment_5/images/image_lines_3.png"
image_path = "./images/"


def set_threshold(img, threshold):
    height, width = img.shape  # (480, 640) as default
    img_bw = img

    for i in range(height):  # Zeilenindex
        for j in range(width):  # Spaltenindex
            if img_bw[i][j] >= threshold:  # threshold = 200 z.B.
                img_bw[i][j] = 1  # set white
            else:
                img_bw[i][j] = 0  # set black

    return img_bw


def manuel_sift(img):
    """
    245 ist schon das richtige Threshold in unserem
    Fall. Dennoch sind die zwei Lampen der zwei anderen
    Modelcars noch manuell auszusieben.
    """
    h, w = img.shape

    for i in range(h):
        for j in range(w):
            if i < 0.45 * h:  # die vordere Lampe aussieben
                img[i][j] = 0
            if j < 0.15 * w:  # die linke Lampe aussieben
                img[i][j] = 0

    return img


def RANSAC(bin_img, img_raw):
    X = []
    y = []
    last_pixel_white = False
    for row in range(len(bin_img)):
        for column in range(len(bin_img[row])):
            if (bin_img[row][column] == 1):

                if not last_pixel_white:
                    X.append([column])
                    y.append([row])
                    last_pixel_white = True
            else:
                last_pixel_white = False

    amount_of_lines = 2
    X = np.array(X)
    y = np.array(y)
    # y, X = np.nonzero(bin_img)
    X = X.reshape(-1, 1)
    y = y.reshape(-1, 1)
    lines = []
    for i in range(amount_of_lines):
        ransac = linear_model.RANSACRegressor(residual_threshold = 5.0)
        ransac.fit(X, y)
        inlier_mask = ransac.inlier_mask_
        outlier_mask = np.logical_not(inlier_mask)


        line_X = np.arange(X.min(), X.max())[:, np.newaxis]
        line_y_ransac = ransac.predict(line_X)
        cv.line(img_raw, (line_X[0],line_y_ransac[0]), (line_X[-1], line_y_ransac[-1]), (0,255,0), thickness=1, lineType=8, shift=0)
        X = np.delete(X, np.where(inlier_mask), 0)
        y = np.delete(y,np.where(inlier_mask), 0)


        # Calculation of line equation
        points = [(line_X[0][0], line_y_ransac[0][0]), (line_X[-1][0], line_y_ransac[-1][0])]
        x_coords, y_coords = zip(*points)
        A = np.vstack([x_coords, np.ones(len(x_coords))]).T
        m, c = np.linalg.lstsq(A, y_coords)[0]
        lines.append((m,c))



    # show results
    print(lines)
    cv.imshow("Image with lines",img_raw)
    cv.waitKey(0)






def main(threshold):
    img_bgr = cv.imread(image_raw)
    img = cv.cvtColor(img_bgr, cv.COLOR_BGR2GRAY)
    img_bw = set_threshold(img, threshold)
    img_bw = manuel_sift(img_bw)
    RANSAC(img_bw,img_bgr)




if __name__ == "__main__":
    main(THRESHOLD)
