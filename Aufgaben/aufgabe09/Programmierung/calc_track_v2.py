#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 16 22:44:52 2019

@author: Phú & Myz
"""


import cv2
#from matplotlib import pyplot as plt
import numpy as np


blue = [255, 0, 0]      # für den gegebenen Punkt
green = [0, 255, 0]     # doch besser, für die gegebenen Punkte
rot = [0, 0, 255]       # für den zutreffenden Punkt


Mittelpunkt_K1 = (195, 215)
Mittelpunkt_K2 = (405, 215)
H_G1 = 95               # Höhe von Gerade 1
H_G2 = 335              # Höhe von Gerade 2
Radius = 120

X_MIN, X_MAX = 0, 600
Y_MIN, Y_MAX = 0, 430

linke_grenze, rechte_grenze, mittle_grenze = 195, 405, 215

gegebene_liste = [(0, 0), (200, 400), (100, 300)]


# /////////////////////////////////////////////////////////////////////////////

ordner_path = "./images/"
ziel_path = ordner_path + "aufgabe2/"


#img_gedreht = cv2.imread(ordner_path + "map_gedreht.png", cv2.IMREAD_GRAYSCALE)
#img_geteilt = cv2.imread(ordner_path + "map_aufgeteilt_markiert_v2.png")



def closest_point_to_a_line(punkt, h):
    """
    Die gegebene Gerade: y = h
    """
    x, y = punkt
    P_x = int(round(x))
    
    return (P_x, h)


def closest_point_to_a_halfcircle(punkt, mittelpunkt, r):
    """
    (M_x, M_y) ist der Mittelpunkt des Kreises, und r ist der Radius
    """
    x, y = punkt
    M_x, M_y = mittelpunkt
    
    d = np.sqrt((x - M_x)**2 + (y - M_y)**2)
    P_x = int(round(r*(float(x - M_x)/d) + M_x))
    P_y = int(round(r*(float(y - M_y)/d) + M_y))
    
    return (P_x, P_y)


def find_closest_point(punkt):
    x, y = punkt
    
    # Überprüfe punkt zunächst, ob er gültig ist
    if x < X_MIN or x > X_MAX:
        print ("Ungültiges x!")
        return (-1, -1)
    
    if y < Y_MIN or y > Y_MAX:
        print ("Ungültiges y!")
        return (-1, -1)
    
    
    # Berechne den zutreffenden Punkt je nach dem vier Bereichen
    if x < linke_grenze:
        ans = closest_point_to_a_halfcircle(punkt, Mittelpunkt_K1, Radius)
        return ans
    elif x > rechte_grenze:
        ans = closest_point_to_a_halfcircle(punkt, Mittelpunkt_K2, Radius)
        return ans
    elif y < mittle_grenze:
        ans = closest_point_to_a_line(punkt, H_G1)
        return ans
    else:
        ans = closest_point_to_a_line(punkt, H_G2)
    
    
    return ans


def zeichne_Punkte_und_deren_entsprechende_Punkte_auf_der_Bahn(img, punkt_liste, etikett = "default"):
    for punkt in punkt_liste:
        if punkt == (-1, -1):           # Überspringe die Fehler
            continue
        
        ans = find_closest_point(punkt)
        cv2.circle(img, umrechne_punkt(ans), 3, rot)
        cv2.circle(img, umrechne_punkt(punkt), 3, green)
    
    cv2.imshow("default", img)
    cv2.waitKey(0)
    cv2.imwrite(ziel_path + "punkte_markiert_" + etikett + ".png", img)
    
#    return img

def umrechne_punkt(punkt):
    """
    y muss noch umgerechnet werden, indem y = Y_MAX - y,
    bevor punkt gezeichnet wird
    """
    x, y = punkt
    return (x, Y_MAX - y)


        
def main(punkt_liste, etikett="default"):

#    punkt_liste = [Mittelpunkt_K1, Mittelpunkt_K2, (300, 215)]
#    punkt_liste = [(53, 25), (375, 111), (267, 420), (559, 301)]
#    punkt_liste = [(600, 300)]
#    punkt_liste = gegebene_liste
    img = cv2.imread(ordner_path + "map_aufgeteilt_markiert_v2.png")
    zeichne_Punkte_und_deren_entsprechende_Punkte_auf_der_Bahn(img, punkt_liste, etikett = etikett)
        
        

if __name__ == "__main__":
    main(gegebene_liste, etikett = "gegebene_liste")
    main([Mittelpunkt_K1, Mittelpunkt_K2, (300, 215)], etikett = "mittelpunkte")
    main([(53, 25), (375, 111), (267, 420), (559, 301)], etikett = "weitere")

