#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 16 22:44:52 2019

@author: myz
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from calc_track_v2 import find_closest_point


daten_path = "./Rohdaten/"
ergebnis_path = "./Ergebnisse/"
daten_name = "rohdaten.csv"

datei = daten_path + daten_name


df = pd.read_csv(datei)


x_liste = np.array(df['x'])
y_liste = np.array(df['y'])

X = np.array(range(len(x_liste)))


# /////////////////////////// Position-Plotting ////////////////////////////



def plotting():
    s_width = 1
    
    X_liste = list(df['x'])
    Y_liste = list(df['y'])
    colors = cm.rainbow(np.linspace(0, 1, len(X_liste)))
    
    plt.scatter(X_liste, Y_liste, s=s_width, color=colors)
    plt.xlabel("X-Achse")
    plt.ylabel("Y-Achse")
    plt.savefig(ergebnis_path + "fahrtspur_v1.png", dpi=300)
    
    plt.show()
    plt.close()

# //////////////////////////////////////////////////////////////////////////
    

def berechne_absolute_distance(punkt, richtiger_punkt):
    x, y = punkt
    x_0, y_0 = richtiger_punkt
    
    d = np.abs(x - x_0) + np.abs(y - y_0)    
    return d


def berechne_squared_distance(punkt, richtiger_punkt):
    x, y = punkt
    x_0, y_0 = richtiger_punkt
    
    d = (x - x_0)**2 + (y - y_0)**2
    return d


def berechne_MAE(X_liste, Y_liste):
    summe = 0
    for i in range(len(X_liste)):
        x, y = X_liste[i], Y_liste[i]
        richtiger_punkt = find_closest_point((x, y))
        d_i = berechne_absolute_distance((x, y), richtiger_punkt)
        summe += d_i
        
    average = float(summe)/len(X_liste)
    
    return average



def berechne_MSE(X_liste, Y_liste):
    summe = 0
    for i in range(len(X_liste)):
        x, y = X_liste[i], Y_liste[i]
        richtiger_punkt = find_closest_point((x, y))
        d_i = berechne_squared_distance((x, y), richtiger_punkt)
        summe += d_i
        
    average = float(summe)/len(X_liste)
    
    return average


def main():
    X_liste = list(df['x'])
    X_liste = [x*100 for x in X_liste]
    Y_liste = list(df['y'])
    Y_liste = [y*100 for y in Y_liste]
    
    mae = berechne_MAE(X_liste, Y_liste)
    mse = berechne_MSE(X_liste, Y_liste)
    
    print("MAE = {:.2f} cm".format(mae))
    print("MSE = {:.2f} cm^2".format(mse))



if __name__ == "__main__":
    main()






# ////////////////////////////// x-y-Plotting ////////////////////////////////
#
#plt.scatter(X, x_liste, s=0.005, label='x')
#plt.scatter(X, y_liste, s=0.005, label='y')
#
#plt.xlabel("Anzahl der Messung")
#plt.ylabel("Koordinatenwerte (m)")
#plt.legend(loc='lower right')
#plt.title("Veraenderungen der x- und y-Koordinaten waehrend einer Rundfahrt")
#plt.savefig(ergebnis_path + "Veraenderungen_der_x_und_y.png", dpi=300)
#
#plt.show()
#plt.close()
#
# ////////////////////////////////////////////////////////////////////////////





























    
