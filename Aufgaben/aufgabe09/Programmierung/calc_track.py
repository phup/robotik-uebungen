import cv2
from matplotlib import pyplot as plt
import numpy as np
img = cv2.imread("images/map.bmp")



def upper_circle(x):
    return ((-1*(np.sqrt(120**2)-(x-215)**2) + 195))


def lower_circle(x):
    return (((np.sqrt(120**2)-(x-215)**2) + 405))

def closest_point_to_upper_circle(x,y):
    if x == 215 and y == 195:
        cx = 95
        cy = 185
    else:
        cx = (215 + 120 * ((x - 215) / np.sqrt((x - 215) ** 2 + (y - 195) ** 2)))
        cy = (195 + 120 * ((y - 195) / np.sqrt((x - 215) ** 2 + (y - 195) ** 2)))

    return (int(cx),int(cy))

def closest_point_to_lower_circle(x, y):
    if x == 215 and y == 405:
        cx = 335
        cy = 405
    else:
        cx = (215 + 120 * ((x - 215) / np.sqrt((x - 215) ** 2 + (y - 405) ** 2)))
        cy = (405 + 120 * ((y - 405) / np.sqrt((x - 215) ** 2 + (y - 405) ** 2)))

    return (int(cx),int(cy))
def closest_point(x,y):
    if y > 0 and y <= 185:
        closest_point = closest_point_to_upper_circle(x,y)

    if y > 185 and y <= 415:
        if np.abs(95 - x) < np.abs(335 - x):
            closest_point = (95,y)
        else:
            closest_point = (335,y)
    if y > 415:
        closest_point = closest_point_to_lower_circle(x,y)

    return closest_point




pointx, pointy = 129,100
print(pointy)
closest_point = closest_point(pointx,pointy)
print(closest_point)
cv2.circle(img,closest_point, 3, (255,0,0))
cv2.circle(img,(pointx,pointy), 3,(0,255,0))

cv2.namedWindow('image',cv2.WINDOW_NORMAL)
cv2.resizeWindow('image', 1000,1000)
cv2.imshow("image",img)
cv2.waitKey(0)
