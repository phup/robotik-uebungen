#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 17 19:42:38 2019

@author: myz
"""


import cv2
from matplotlib import pyplot as plt
import numpy as np

ordner_path = "./images/"
img_path = "./images/map_aufgeteilt_markiert.png"
#img_path_2 = "./images/map.bmp"


img = cv2.imread(img_path)
#img_2 = cv2.imread(img_path_2)


base = 30
farbe_1 = [base, base*2, 0]
farbe_2 = [base*2, base, 0]
farbe_3 = [base, 0, base*2]
farbe_4 = [base*2, 0, base]

weisse_farbe = [255, 255, 255]
schwarze_farbe = [0, 0, 0]

x_linke_grenze = 195
x_rechte_grenze = 405
mittele_grenze = 215


def schwarz_weiss_invertieren(img):
    zeilen_anzahl, spalten_anzahl, tmp = img.shape
    for i in range(zeilen_anzahl):
        for j in range(spalten_anzahl):
            # konvertiere schwarz zum weiß
            if list(img[i][j]) == weisse_farbe:
                img[i][j] = schwarze_farbe
                continue
            # konvertiere weiß zum schwarz
            if list(img[i][j]) == schwarze_farbe:
                img[i][j] = weisse_farbe
    
    return img


# Test
#img_invert = schwarz_weiss_invertieren(img)
#print(img_invert)
#cv2.imshow("test", img_invert)
#cv2.waitKey(0)
#cv2.imwrite(ordner_path + "map_invertiert.png", img_invert)
    
def map_aufteilen(img):
    zeilen_anzahl, spalten_anzahl, tmp = img.shape
    for x in range(spalten_anzahl):
        for y in range(zeilen_anzahl):
            pixel_farbe = img[y][x]
            
            if list(pixel_farbe) == schwarze_farbe:
                continue                # übersprünge die Bahnlinien!
                
            if x < 195:
                img[y][x] = farbe_1     # zum K1 gehört
                continue
            elif x > 405:
                img[y][x] = farbe_2     # zum K2 gehört
                continue
            elif y < 215:
                img[y][x] = farbe_3     # zum G1 gehört
                continue
            else:
                img[y][x] = farbe_4     # zum G2 gehört
                continue
                
    return img

# Test
#img_aufgeteilt = map_aufteilen(img_invert)
#cv2.imshow("test2", img_aufgeteilt)
#cv2.waitKey(0)
#cv2.imwrite(ordner_path + "map_aufgeteilt.png", img_invert)
    


# Laufen für v2
img_aufgeteilt = map_aufteilen(img)
cv2.imshow("test_v2", img_aufgeteilt)
cv2.waitKey(0)
cv2.imwrite(ordner_path + "map_aufgeteilt_markiert_v2.png", img_aufgeteilt)
            
            


            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
    