#!/usr/bin/env python
import sys
import rospy
import cv2
import numpy as np
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Twist
from std_msgs.msg import String
from geometry_msgs.msg import Pose
from geometry_msgs.msg import Point
from geometry_msgs.msg import Quaternion
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
from operator import itemgetter
from std_msgs.msg import Float32
from nav_msgs.msg import Odometry
from math import sqrt
from std_msgs.msg import UInt8
from std_msgs.msg import Int16
#import matplotlib
#matplotlib.use('Agg')
#from matplotlib import pyplot as plt
"""
# --- definitions ---
last_driving_control_info = ""
epsilon = 0.05   # allowed inaccuracy for distance calculation
angle_left = 0
angle_straight = 86
angle_right = 180
last_odom = None
is_active = False
distance = 0.2
"""

start_angle = 86


# from __future__ import print_function
def threshold_line(img):
    ret, thresh = cv2.threshold(img, 200, 255, cv2.THRESH_BINARY)
    return thresh


class image_converter:

  def __init__(self):
    self.image_pub = rospy.Publisher("/image_processing/bin_img",Image, queue_size=100)
    self.bridge = CvBridge()
    self.error_array = []
    self.change_counter = 0
    self.previous_angle_change = 0
    self.all_angles = []
    # self.sub_twist = rospy.Subscriber("/twist",Twist ,self.twistCallback, queue_size=100)  
    self.image_sub = rospy.Subscriber("/camera/color/image_raw",Image,self.image_callback, queue_size=100)
    self.current_rpm = 0
    self.last_rpm = 0
    self.is_first_rpm = False
    self.last_error = 0
    self.p_weight = 0.3
    self.d_weight = 0.2
    self.iterations = -1
    self.target_v = 0.3
    self.target_rpm = berechne_rpm(self.target_v)
    self.current_speed = 150
    self.nearest_point_to_track = []
    self.car_positions = []
    self.position_sub = rospy.Subscriber("/localization/odom/11",Odometry,self.position_callback, queue_size=100)
    self.f = open('./rohdaten.txt','w')
  def position_callback(self,msg):
      x = msg.pose.pose.position.y
      y = msg.pose.pose.position.x

      # ////////////////////////////////////
      #f = open('rohdaten.txt', 'w')
      one_line = str(x) + ", " + str(y) + "\n"
      self.f.write(one_line)
      # ////////////////////////////////////


      nearest_point = calc_closest_point(x*100,y*100)
      self.car_positions.append((int(x*100),int(y*100)))
      self.nearest_point_to_track.append(nearest_point)
      #print(len(self.car_positions))
      #print("Closest_Points")
      #print(self.nearest_point_to_track)

  def twistCallback(self,msg):
      #print(last_rpm)
      self.iterations +=1
      #print(self.iterations%10)
      #print(iterations%10)
      if self.is_first_rpm == False:
        self.current_rpm = msg.linear.x
        self.is_first_rpm = True
      elif self.iterations%10 == 0 :
        self.last_rpm = self.current_rpm
        self.last_error  =  self.target_rpm - self.last_rpm
        self.current_rpm = msg.linear.x
        error = self.target_rpm - self.last_rpm
        change = (self.current_rpm - self.last_rpm)/0.1
        u = self.p_weight*error + self.d_weight*(-1 * change)
        # print(error)
        if self.current_rpm != 0:
          self.current_speed += u
        #print(current_speed,current_rpm, u)
        #print(self.current_rpm)
        pub_speed.publish(self.current_speed)

  def image_callback(self,data):
    try:
      cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
      cv_image_height_middle = int(len(cv_image) *0.75)
      cv_image_width_middle = int(len(cv_image[0]) / 2)
      gray_image = cv2.cvtColor(cv_image, cv2.COLOR_BGR2GRAY)
      binary_img = threshold_line(gray_image)
      optimal_angle = 0
    except CvBridgeError as e:
      print(e)
    current_angle = start_angle
    last_min=9999
    pub_speed.publish(150)
    for i in range(len(binary_img[cv_image_height_middle])):
        pixel=binary_img[cv_image_height_middle][i]
        if pixel>0:
	    if abs(i-cv_image_width_middle)<last_min:
                last_min=i
    if last_min!=9999:
      line_equation_height_middle=last_min
      cv2.circle(cv_image,(line_equation_height_middle,cv_image_height_middle),9,(255,0,255))
      error = line_equation_height_middle - cv_image_width_middle
      steering_angle_change = 0.5*(error)
      self.all_angles.append(steering_angle_change)
      if steering_angle_change * self.previous_angle_change < 0:
         self.error_array = []
      else:
        self.error_array.append(steering_angle_change)
        self.change_counter += 1
        if self.change_counter == 10:
          mean_error = np.median(self.error_array)
          current_angle =  start_angle + mean_error
	  if current_angle > 180:
	    current_angle = 180
	  if current_angle < 0:
	    current_angle = 0
          self.change_counter = 0
	  self.error_array = []
          pub_steering.publish(current_angle)
    try:
      self.image_pub.publish(self.bridge.cv2_to_imgmsg(cv_image, "bgr8"))
    except CvBridgeError as e:
      print(e)


def callbackOdom(msg):
    global last_odom
    last_odom = msg
def waitForFirstOdom():
    while not rospy.is_shutdown() and last_odom is None:
        rospy.loginfo(
            "%s: No initial odometry message received. Waiting for message...",
            rospy.get_caller_id())
        rospy.sleep(1.0)

def callbackDrivingControl(msg):
    last_driving_control_info = msg.data

def change_angle(distance, command, speed, angle):
    pub_stop_start.publish(1)
    rospy.sleep(1)
    pub_steering.publish(angle)
    pub_stop_start.publish(0)
    rospy.sleep(1)
    pub_speed.publish(speed)

def closest_point_to_upper_circle(x,y):
    if x == 215 and y == 195:
        cx = 95
        cy = 185
    else:
        cx = (215 + 120 * ((x - 215) / np.sqrt((x - 215) ** 2 + (y - 195) ** 2)))
        cy = (195 + 120 * ((y - 195) / np.sqrt((x - 215) ** 2 + (y - 195) ** 2)))

    return (int(cx),int(cy))

def closest_point_to_lower_circle(x, y):
    if x == 215 and y == 405:
        cx = 335
        cy = 405
    else:
        cx = (215 + 120 * ((x - 215) / np.sqrt((x - 215) ** 2 + (y - 405) ** 2)))
        cy = (405 + 120 * ((y - 405) / np.sqrt((x - 215) ** 2 + (y - 405) ** 2)))

    return (int(cx),int(cy))

def calc_closest_point(x,y):
    if y >= 0 and y <= 185:
        closest_point = closest_point_to_upper_circle(x,y)

    if y > 185 and y <= 415:
        if np.abs(95 - x) < np.abs(335 - x):
            closest_point = (95,y)
        else:
            closest_point = (335,y)
    if y > 415:
        closest_point = closest_point_to_lower_circle(x,y)

    return closest_point



alpha = 0.5
radius = 0.03

def berechne_rpm(v):
    v = v/(1+alpha)
    rpm = (60.0*v) / (2*np.pi*radius)
    return rpm

rospy.init_node('image_converter', anonymous=True)

pub_stop_start = rospy.Publisher("manual_control/stop_start",Int16,queue_size=100)
pub_speed = rospy.Publisher("manual_control/speed", Int16, queue_size=100)
pub_steering = rospy.Publisher("steering",UInt8,queue_size=100)
pub_info = rospy.Publisher("simple_drive_control/info", String, queue_size=100)  
ic = image_converter() 



try:
  rospy.spin()
except KeyboardInterrupt:
  print("Shutting down")
cv2.destroyAllWindows()


