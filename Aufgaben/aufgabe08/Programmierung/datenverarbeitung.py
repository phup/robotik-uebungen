# -*- coding: utf-8 -*-
"""
Created on Fri Jan 11 01:26:33 2019

@author: Myz
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


df_0_5 = pd.read_csv("0_5.csv")
df_0_8 = pd.read_csv("0_8.csv")

def my_plot(df, label):
    X = np.array(df.index)
    y = np.array(df["rpm"])
    
    plt.plot(X, y, label=label)

    
    
    
    
    
def main():
    my_plot(df_0_5, "v = 0.5 m/s")
    my_plot(df_0_8, "v = 0.8 m/s")
    
    plt.title("Das Plotting für Aufgabe 1")
    plt.xlabel("Anzahl der Messungen")
    plt.ylabel('rpm')
    
    plt.legend(loc='lower right')
    plt.savefig("unser_Plotting_v1.png")
    plt.show()
    plt.close()
    
    
if __name__ == "__main__":
    main()
    