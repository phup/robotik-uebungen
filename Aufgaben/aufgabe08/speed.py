# -*- coding: utf-8 -*-
"""
Created on Thu Jan 10 13:33:16 2019

@author: Lenovo
"""
import numpy as np


radius = 0.03
alpha = 0.5

def berechne_velocity(rpm):
    v = (2*np.pi*radius* rpm)/(60.0)
    return v

def berechne_rpm(v):
    v = v/(1+alpha)
    rpm = (60.*v) / (2*np.pi*radius)
    return rpm