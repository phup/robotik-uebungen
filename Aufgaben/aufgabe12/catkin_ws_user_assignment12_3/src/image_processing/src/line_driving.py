#!/usr/bin/env python

import sys
import rospy
import cv2
import numpy as np
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Twist
from std_msgs.msg import String
from geometry_msgs.msg import Pose
from geometry_msgs.msg import Point
from geometry_msgs.msg import Quaternion
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
from operator import itemgetter
from std_msgs.msg import Float32
from nav_msgs.msg import Odometry
from math import sqrt
from std_msgs.msg import UInt8
from std_msgs.msg import Int16
import math
from tf.transformations import euler_from_quaternion
import sensor_msgs.msg

# import matplotlib
# matplotlib.use('Agg')
# from matplotlib import pyplot as plt
"""
# --- definitions ---
last_driving_control_info = ""
epsilon = 0.05   # allowed inaccuracy for distance calculation
angle_left = 0
angle_straight = 86
angle_right = 180
last_odom = None
is_active = False
distance = 0.2
"""

start_angle = 86


# from __future__ import print_function
def threshold_line(img):
    ret, thresh = cv2.threshold(img, 200, 255, cv2.THRESH_BINARY)
    return thresh


class image_converter:

    def __init__(self):
        # self.image_pub = rospy.Publisher("/image_processing/bin_img", Image, queue_size=100)
        self.bridge = CvBridge()
        self.change_counter = 0
        self.all_angles = []
        # self.sub_twist = rospy.Subscriber("/twist",Twist ,self.twistCallback, queue_size=100)
        self.current_rpm = 0
        self.last_rpm = 0
        self.is_first_rpm = False
        self.last_error = 0
        self.p_weight = 0.3
        self.d_weight = 0.2
        self.iterations = -1
        self.target_v = 0.3
        self.target_rpm = berechne_rpm(self.target_v)
        self.current_speed = 150
        self.position_sub = rospy.Subscriber("/localization/odom/8", Odometry, self.position_callback, queue_size=100)
        self.laser_sub = rospy.Subscriber("/scan", sensor_msgs.msg.LaserScan, self.callbackScanData, queue_size=100)
        # pub_speed.publish(180)
        self.previous_angle = 0
        self.angle_p_weight = 2.
        self.angle_d_weight = 0.6
        self.laneID = 1
        self.lookDistance = 30
        # self.f = open('./rohdaten.txt', 'w')

    def callbackScanData(self, data):

        position_msg = rospy.wait_for_message('/localization/odom/8', Odometry)
        car_x = position_msg.pose.pose.position.x
        car_y = position_msg.pose.pose.position.y
	
        z = position_msg.pose.pose.orientation.z
        w = position_msg.pose.pose.orientation.w
        roll, pitch, yaw = euler_from_quaternion((0, 0, z, w))
        #print("old")
	#print(car_x,car_y)
	car_x -= np.cos(yaw + np.pi) * 0.3
        car_y -= np.sin(yaw + np.pi) * 0.3
	#print("new")
	#print(car_x,car_y)

 	ranges = data.ranges
	obstacle_points = []

        closest_car_point = find_closest_point((car_x * 100, car_y * 100), self.laneID, self.lookDistance)
        closest_car_point = (closest_car_point[0] / 100., closest_car_point[1] / 100.)

        # print(ranges[45:136])
        angle_dict = {}
        min_distance_l1 = 100.
        min_distance_l2 = 100.
        min_point_l1 = (9000., 9000.)
        min_point_l2 = (9000., 9000.)
        distance_to_obstacle_l1 = 100.
        distance_to_obstacle_l2 = 100.
	#print(yaw,ranges[0])
        #print(ranges[0])
        ranges = list(ranges)  # .reverse()
        #ranges.reverse()
        for i in range(len(ranges)):
            if ranges[i] <= 1.5 and ((i >= 0 and i <= 75) or (i >= 285 and i <= 360)):
                angle_dict[str(i)] = ranges[i]
        #print(angle_dict["0"])
        # print(ranges[270])
        for angle, magnitude in angle_dict.items():

            angle_rad = -1. * np.pi + float(angle) * np.pi / 180.
            #print(angle_rad)
	    magnitude = magnitude 
            obstacle_pointy = magnitude * np.sin(angle_rad) 
            obstacle_pointx = magnitude * np.cos(angle_rad) - 0.05
            #print((obstacle_pointx,obstacle_pointy))
	    o_w = calc_world_coord(obstacle_pointx, obstacle_pointy, car_x, car_y, yaw)
            #print(o_w)
	    o_w = np.array((o_w[0], o_w[1]))
            o_w_cm = np.array((o_w[0] * 100, o_w[1] * 100))
            if angle == "0":
	      #print(np.cos(angle_rad))
	      #print(magnitude)
	      #print(magnitude,obstacle_pointx,obstacle_pointy)
	      #print(yaw)
              #print(car_x,car_y)
	      #print(o_w,(car_x,car_y))
              print(" ")
	    # print(o_w_cm)
            closest_point_lane1 = find_closest_point(o_w_cm, 1, 0)
            closest_point_lane2 = find_closest_point(o_w_cm, 2, 0)

            closest_point_lane1 = np.array((closest_point_lane1[0] / 100., closest_point_lane1[1] / 100.))
            closest_point_lane2 = np.array((closest_point_lane2[0] / 100., closest_point_lane2[1] / 100.))
            obstacle_points.append(list(o_w))
            distance_to_closest_point_l1 = np.linalg.norm(o_w - closest_point_lane1)
            distance_to_closest_point_l2 = np.linalg.norm(o_w - closest_point_lane2)
            # if distance_to_closest_point_l1 < 0.15 and distance_to_closest_point_l2 < 0.15:
            # print(distance_to_closest_point_l1,distance_to_closest_point_l2)
            if distance_to_closest_point_l1 < 0.1:
                distance_to_obstacle_l1 = np.linalg.norm(np.array(closest_car_point) - np.array(closest_point_lane1))
                if distance_to_obstacle_l1 < min_distance_l1:
                    min_distance_l1 = distance_to_obstacle_l1
                    min_point_l1 = closest_point_lane1
            if distance_to_closest_point_l2 < 0.1:
                distance_to_obstacle_l2 = np.linalg.norm(np.array(closest_car_point) - np.array(closest_point_lane2))
                if distance_to_obstacle_l2 < min_distance_l2:
                    min_distance_l2 = distance_to_obstacle_l2
                    min_point_l2 = closest_point_lane2
	#print(min_point_l2,min_point_l1,(car_x,car_y) )
        #print(min_distance_l1, min_distance_l2)
        # closest_car_point = find_closest_point((car_x * 100, car_y * 100), self.laneID, self.lookDistance)
        # closest_car_point= (closest_car_point[0]/100,closest_car_point[1]/100)
        # distance_to_obstacle_l1 = np.linalg.norm(np.array(closest_car_point) - np.array(min_point_l1))
        # distance_to_obstacle_l2 = np.linalg.norm(np.array(closest_car_point) - np.array(min_point_l2))
        # print(min_point_l1,min_point_l2)
        #print(min_distance_l1, min_distance_l2)
	print(closest_car_point, (car_x,car_y))
	#print(obstacle_points)
	max_distance = 0.5
        if min_distance_l1 <= max_distance and min_distance_l2 <= max_distance:
            pub_speed.publish(0)
            print("stop \n")
        elif min_distance_l1 <= max_distance:
            self.laneID = 2
            print("change2lane2 \n")
            self.all_angles = []
        elif min_distance_l2 <= max_distance:
            self.laneID = 1
	    self.all_angles = []
            print("change2lane1 \n")
	else:
	    print("no obstacle \n")
    def position_callback(self, msg):
        pub_speed.publish(200)
        x = msg.pose.pose.position.x
        y = msg.pose.pose.position.y
        z = msg.pose.pose.orientation.z
        w = msg.pose.pose.orientation.w
        roll, pitch, yaw = euler_from_quaternion((0, 0, z, w))
	x -=  np.cos(yaw + np.pi) * 0.3
        y -= np.sin(yaw + np.pi) * 0.3

	#print(x,y)
        xcm = x * 100
        ycm = y * 100
        # print(xcm,ycm)
        # print(find_closest_point((69,281),1,50))
        closest_point = np.array(find_closest_point((xcm, ycm), self.laneID, self.lookDistance))
        # print(xcm,ycm)
        # print(closest_point)
        numpy_point = np.array((xcm, ycm))

        closest_point_vector = closest_point - numpy_point
        vector = np.array((1, 0))
        # print(closest_point_vector)
        # print((closest_point_vector))
        # print(vector)
        closest_point_error = np.arccos(
            np.dot(vector, closest_point_vector) / (np.linalg.norm(vector) * np.linalg.norm(closest_point_vector)))

        # print(yaw,closest_point_error)
        if closest_point_vector[1] >= 0:
            closest_point_error = closest_point_error * -1  # + np.pi
        else:
            closest_point_error = closest_point_error  # + np.pi
        error = closest_point_error + yaw
        if error > np.pi:
            error -= 2 * np.pi
        elif error < -1 * np.pi:
            error += 2 * np.pi
        # print(closest_point_vector)
        # print(-1*closest_point_vector[1])
        # print(closest_point_error,yaw)
        # print (error)
        # error = (error/np.pi)*180 + 86
        # print(error)
        # print(error)
        self.previous_angle = error

        steering_angle = self.angle_p_weight * (error) + self.angle_d_weight * (error - self.previous_angle)
        steering_angle = steering_angle / np.pi * 180 + 86
        self.all_angles.append(steering_angle)
        # print(steering_angle)
        self.change_counter += 1
        if self.change_counter == 5:
            mean_error = np.median(self.all_angles)
            current_angle = mean_error
            if current_angle > 180:
                current_angle = 180
            if current_angle < 0:
                current_angle = 0
            self.change_counter = 0
            self.all_angles = []
            # print(current_angle)
            pub_steering.publish(current_angle)

    def twistCallback(self, msg):
        # print(last_rpm)
        self.iterations += 1
        # print(self.iterations%10)
        # print(iterations%10)
        if self.is_first_rpm == False:
            self.current_rpm = msg.linear.x
            self.is_first_rpm = True
        elif self.iterations % 10 == 0:
            self.last_rpm = self.current_rpm
            self.last_error = self.target_rpm - self.last_rpm
            self.current_rpm = msg.linear.x
            error = self.target_rpm - self.last_rpm
            change = (self.current_rpm - self.last_rpm) / 0.1
            u = self.p_weight * error + self.d_weight * (-1 * change)
            # print(error)
            if self.current_rpm != 0:
                self.current_speed += u
            # print(current_speed,current_rpm, u)
            # print(self.current_rpm)
            pub_speed.publish(self.current_speed)


def callbackOdom(msg):
    global last_odom
    last_odom = msg


def waitForFirstOdom():
    while not rospy.is_shutdown() and last_odom is None:
        rospy.loginfo(
            "%s: No initial odometry message received. Waiting for message...",
            rospy.get_caller_id())
        rospy.sleep(1.0)


def callbackDrivingControl(msg):
    last_driving_control_info = msg.data


def calc_world_coord(o_x, o_y, c_x, c_y, car_orientation):
    car_orientation = car_orientation  -  np.pi 
    cos = np.cos(car_orientation)
    sin = np.sin(car_orientation)
    transformation_matrix = np.array(
        [[cos, -1. * sin, 0., c_x], [sin, cos, 0., c_y], [0., 0., 1., 0.], [0., 0., 0., 1.]])
    o_w = np.dot(transformation_matrix, np.array([o_x, o_y, 0, 1]))
    return o_w


def change_angle(distance, command, speed, angle):
    pub_stop_start.publish(1)
    rospy.sleep(1)
    pub_steering.publish(angle)
    pub_stop_start.publish(0)
    rospy.sleep(1)
    pub_speed.publish(speed)


Mittelpunkt_K1 = (195, 215)
Mittelpunkt_K2 = (405, 215)

Radius_inner_circle = 135
Radius_outer_circle = 165
H_G1_L1 = 80
H_G2_L1 = 350

H_G1_L2 = 50
H_G2_L2 = 380

X_MIN, X_MAX = 0, 600
Y_MIN, Y_MAX = 0, 430
linke_grenze, rechte_grenze, mittle_grenze = 185, 415, 215


def closest_point_to_a_line(punkt, h):
    """
    Die gegebene Gerade: y = h
    """
    x, y = punkt
    P_x = x
    return (P_x, h)


def closest_point_to_a_halfcircle(punkt, mittelpunkt, r):
    """
    (M_x, M_y) ist der Mittelpunkt des Kreises, und r ist der Radius
    """
    x, y = punkt
    M_x, M_y = mittelpunkt

    d = np.sqrt((x - M_x) ** 2. + (y - M_y) ** 2.)
    P_x = (r * (float(x - M_x) / d) + M_x)
    P_y = (r * (float(y - M_y) / d) + M_y)

    return (P_x, P_y)


def calc_closest_point_with_distance_for_circles(punkt, lane, distance, circle_ID):
    if lane == 1:
        radius = Radius_inner_circle
    if lane == 2:
        radius = Radius_outer_circle

    if circle_ID == 1:
        center = Mittelpunkt_K1
        ansx, ansy = closest_point_to_a_halfcircle(punkt, center, radius)
        # print("test")
        # print(ansx,ansy,center[1])
        # print("test")
        # print(radius)
        # print(distance)
        a = np.arccos((center[1] - ansy) / radius) - float(distance) / radius
        # print(a)

        # print(center[1]-ansy)

        ansx = center[0] - np.sin(a) * radius
        ansy = center[1] - np.cos(a) * radius

        # print(punkt,lane,distance,circle_ID)

        if ansx > linke_grenze:
            circle_distance = np.arccos((center[1] - ansy) / radius) * radius

            if lane == 1:
                height = H_G1_L1
            if lane == 2:
                height = H_G1_L2
            ans = calc_closets_point_with_distance_for_line((linke_grenze, height), lane, distance - circle_distance, 1)

            return (int(ans[0]), int(ans[1]))

    if circle_ID == 2:
        center = Mittelpunkt_K2
        ansx, ansy = closest_point_to_a_halfcircle(punkt, center, radius)
        a = np.arccos((center[1] - ansy) / radius) + float(distance) / radius
        ansx = center[0] + np.sin(a) * radius
        ansy = center[1] - np.cos(a) * radius

        if ansx < rechte_grenze:
            circle_distance = (np.pi - np.arccos((center[1] - ansy) / radius)) * radius

            if lane == 1:
                height = H_G2_L1
            if lane == 2:
                height = H_G2_L2
            ans = calc_closets_point_with_distance_for_line((rechte_grenze, height), lane, distance - circle_distance,
                                                            2)

            return (int(ans[0]), int(ans[1]))

    return (int(ansx), int(ansy))


def calc_closets_point_with_distance_for_line(punkt, lane, distance, line_ID):
    if line_ID == 1:
        if lane == 1:
            line_height = H_G1_L1
        if lane == 2:
            line_height = H_G1_L2
        ans = closest_point_to_a_line(punkt, line_height)
        ans = (ans[0] + distance, ans[1])
        if ans[0] > rechte_grenze:
            ans = calc_closest_point_with_distance_for_circles((rechte_grenze, line_height), lane,
                                                               ans[0] - rechte_grenze, 2)

    if line_ID == 2:
        if lane == 1:
            line_height = H_G2_L1

        if lane == 2:
            line_height = H_G2_L2
        ans = closest_point_to_a_line(punkt, line_height)
        ans = (ans[0] - distance, ans[1])

        if ans[0] < linke_grenze:
            ans = calc_closest_point_with_distance_for_circles((linke_grenze, line_height), lane, linke_grenze - ans[0],
                                                               1)

    return ans


def find_closest_point(punkt, lane, distance):
    x, y = punkt

    # Ueberpruefe punkt zunaechst, ob er gueltig ist
    # print(x,y)
    if x < X_MIN or x > X_MAX:
        # print("Ungueltiges x!")
        return (9000., 9000.)

    if y < Y_MIN or y > Y_MAX:
        # print("Ungueltiges y!")
        return (9000., 9000.)

    # Berechne den zutreffenden Punkt je nach dem vier Bereichen
    if x < linke_grenze:
        ans = calc_closest_point_with_distance_for_circles(punkt, lane, distance, 1)
        return ans
    elif x > rechte_grenze:
        ans = calc_closest_point_with_distance_for_circles(punkt, lane, distance, 2)
        return ans

    elif y < mittle_grenze:
        ans = calc_closets_point_with_distance_for_line(punkt, lane, distance, 1)
        return ans
    else:
        ans = calc_closets_point_with_distance_for_line(punkt, lane, distance, 2)

    return ans


alpha = 0.5
radius = 0.03


def berechne_rpm(v):
    v = v / (1 + alpha)
    rpm = (60.0 * v) / (2 * np.pi * radius)
    return rpm


rospy.init_node('image_converter', anonymous=True)

pub_stop_start = rospy.Publisher("manual_control/stop_start", Int16, queue_size=100)
pub_speed = rospy.Publisher("manual_control/speed", Int16, queue_size=100)
pub_steering = rospy.Publisher("steering", UInt8, queue_size=100)
pub_info = rospy.Publisher("simple_drive_control/info", String, queue_size=100)
ic = image_converter()

def stop_car():
  print("car_stopped")
  pub_speed.publish(0)


try:
    rospy.spin()
    rospy.on_shutdown(stop_car)
except rospy.ROSInterruptException:
    print("Shutting down")
cv2.destroyAllWindows()

