import rospy
import numpy as np
from geometry_msgs.msg import Twist
from std_msgs.msg import Int16

alpha = 0.5
radius = 0.03


class SpeedControl:
    def __init__(self):
        self.sub_twist = rospy.Subscriber("/twist", Twist, self.twist_callback, queue_size=100)
        self.sub_target_speed = rospy.Subscriber("/target_speed", Int16, self.speed_change_callback, queue_size=1)
        self.pub_speed = rospy.Publisher("manual_control/speed", Int16, queue_size=100)
        self.iterations = -1
        self.target_v = 0.3
        self.current_rpm = 0
        self.target_rpm = speed_to_rpm(self.target_v)
        self.current_speed = 150
        self.alpha = 0.5
        self.last_rpm = 0
        self.is_first_rpm = False
        self.last_error = 0
        self.p_weight = 0.3
        self.d_weight = 0.2

    def twist_callback(self, msg):
        # print(last_rpm)
        self.iterations += 1
        # print(self.iterations%10)
        # print(iterations%10)
        if not self.is_first_rpm:
            self.current_rpm = msg.linear.x
            self.is_first_rpm = True
        elif self.iterations % 10 == 0:
            self.last_rpm = self.current_rpm
            self.last_error = self.target_rpm - self.last_rpm
            self.current_rpm = msg.linear.x
            error = self.target_rpm - self.last_rpm
            change = (self.current_rpm - self.last_rpm) / 0.1
            u = self.p_weight * error + self.d_weight * (-1 * change)
            # print(error)
            if self.current_rpm != 0:
                self.current_speed += u
            # print(current_speed,current_rpm, u)
            # print(self.current_rpm)
            self.pub_speed.publish(self.current_speed)

    def speed_change_callback(self, msg):
        self.target_rpm = speed_to_rpm(msg)


def speed_to_rpm(v):
    v2 = v / (1 + alpha)
    rpm = (60.0 * v2) / (2 * np.pi * radius)
    return rpm


speed_control = SpeedControl()
try:
    rospy.spin()
except KeyboardInterrupt:
    print("Shutting down")