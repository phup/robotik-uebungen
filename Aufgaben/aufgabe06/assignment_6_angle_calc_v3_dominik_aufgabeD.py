#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Dec  6 19:51:43 2018

@author: Phu & Myz
"""
import pandas as pd
import math
import numpy as np
from operator import itemgetter


csv_path = "./Rohdaten/"
L = 0.261


def erstelle_angle_dict():
    df_m1 = pd.read_csv(csv_path + "rohdaten_messure_1.csv", index_col=0)
    df_m2 = pd.read_csv(csv_path + "rohdaten_messure_2.csv", index_col=0)
    df_m3 = pd.read_csv(csv_path + "rohdaten_messure_3.csv", index_col=0)
    
    angle_dict = {}
    keys = list(df_m1.columns)
    for key in keys:
        dist_list = []
        angle_dict[key] = dist_list
        for df in (df_m1, df_m2, df_m3):
            dist_tuple = tuple(df[key])
            dist_list.append(dist_tuple)
            
    return angle_dict



def berechne_dist(angle_dict):
    """
    Berechne die Koordinaten (x1,y1), (x2,y2) und (x3, y3) jeweils
    fuer Winkel von 0 bis 179.
    Das Ergebnis wird in dist_dic gespeichert
    """
    dist_dic = {}
    for angle, ranges in angle_dict.items():
        dists=[]
        for Y in ranges:
            first_x=0
            last_x=0
            for x in range(len(Y))[::-1]:
                if Y[x]<100:
                    y2=0
                    for x2 in range(len(Y))[:x-1:-1]:
                        y2=Y[x2]
                        if y2<100:
                            break
                    if abs(Y[x]-y2)>0.5:
                        first_x=x
                        break
            for x in range(len(Y)):
                if Y[x]<100:
                    y2=0
                    for x2 in range(len(Y))[x+1:]:
                        y2=Y[x2]
                        if y2<100:
                            break
                    if abs(Y[x]-y2)>0.5:
                        last_x=x
                        break
           
           
            Y_filtered=Y[first_x:]+Y[:last_x]
    
            Y_smooth=[np.mean([y for y in Y_filtered[x-10:x+10] if y<100]) for x in range(len(Y_filtered))[10:-10]]
            min1=min(enumerate(Y_smooth), key=itemgetter(1))[0]+10
            if min1>len(Y_filtered)/2:
                min2=min1
                min1=min2-90
            else:
                min2=min1+90
            y1=Y_smooth[min1-10]
            y2=Y_smooth[min2-10]
            dists.append((y1,y2))
        dist_dic[angle]=dists
        
    return dist_dic



def berechne_mittelpunkt(p1, p2, p3):
    """
    Berechne den Mittelpunkt mittels 
    p1 = (x1, y1)
    p2 = (x2, y2)
    p3 = (x3, y3)
    """
    (x1, y1) = p1
    (x2, y2) = p2
    (x3, y3) = p3
    
    A = [[2*(x2-x1), 2*(y2-y1)], [2*(x3-x1), 2*(y3-y1)]]
    b = [(x2**2-x1**2+y2**2-y1**2), (x3**2-x1**2+y3**2-y1**2)]
    
    p0 = np.linalg.solve(A, b)
    
    return p0



def berechne_radius(p0, p1):
    """
    Berechne das Radius mittels
    p0 = (x0, y0)   # also der Mittelpunkt
    p1 = (x1, y1)
    """
    (x0, y0) = p0
    (x1, y1) = p1
    r = math.sqrt((x1 - x0)**2 + (y1 - y0)**2)
    return r


def berechne_alle_mittelpunkte(dist_dic):
    centre_dic = {}
    for key in dist_dic.keys():
        (p1, p2, p3) = dist_dic[key]
        centre = berechne_mittelpunkt(p1, p2, p3)
        centre_dic[key] = list(centre)
        
    return centre_dic


def berechne_alle_radien(dist_dic):
    centre_dic = berechne_alle_mittelpunkte(dist_dic)
    radien_dic = {}
    
    for key in dist_dic.keys():
        p0 = centre_dic[key]
        p1 = dist_dic[key][0]
        r = berechne_radius(p0, p1)
        radien_dic[key] = r
        
    return radien_dic


def berechne_winkel(r):
    gamma = np.arctan(L/r)
    gamma = gamma * 180./np.pi
    
    return gamma


def berechne_alle_winkel(radien_dic,dist_dic):
    winkel_dic = {}
    
    for key in radien_dic.keys():
        gamma = berechne_winkel(radien_dic[key])
        direction=1
        if (dist_dic[key][2][0]-dist_dic[key][0][0])/(dist_dic[key][2][1]-dist_dic[key][0][1])>0:
            direction=-1
        winkel_dic[key] = gamma*direction
        
    return winkel_dic

def angle_lookup(angle_dic,angle):
    if angle<angle_dic['0'] or angle>angle_dic['179']:
        print("error: angle out of range")
        return -1
    if angle==angle_dic['0']:
        return 0
    keys=[int(key) for key in angle_dic.keys()]
    keys.sort()
    angles=[angle_dic[str(key)]for key in keys]
    return np.interp(angle,angles,keys)
    
def main():
    angle_dict = erstelle_angle_dict()
    dist_dic = berechne_dist(angle_dict)        # dic fuer p1, p2, p3 fuer alle Winkel
#    centre_dic = berechne_alle_mittelpunkte(dist_dic)       # dic fuer Mittelpunkt fuer alle Winkel
    radien_dic = berechne_alle_radien(dist_dic)
    winkel_dic = berechne_alle_winkel(radien_dic,dist_dic)
    for angle in range(int(winkel_dic['0'])+1,int(winkel_dic['179'])-1,5):
        print("input angle: ",angle,"output: ",angle_lookup(winkel_dic,angle))
    return dist_dic, radien_dic, winkel_dic




if __name__ == "__main__":
    dist_dic, radien_dic, winkel_dic = main()
    
    print("\n* dist_dic:")
    print(dist_dic)
    print("\n* radien_dic:")
    print(radien_dic)
    print("\n* winkel_dic:")
    print(winkel_dic)
