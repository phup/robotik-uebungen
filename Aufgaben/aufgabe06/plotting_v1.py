#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Dec  6 23:05:29 2018

@author: myz
"""

import pandas as pd
import math
import numpy as np
import matplotlib.pyplot as plt


csv_path = "./Rohdaten/"
fig_path = "./Graphen/"
L = 0.261
SIZE = 1.5
MAX = 3.0


def my_plot(X, Y, title, name, druck=True):
    plt.scatter(X, Y, s=SIZE)
    plt.ylim(0, MAX)
    plt.ylabel("distance (m)")
    plt.xlabel("mesaurement")
    plt.title(title)
    
    if druck:
        plt.savefig(name)
        
    plt.show()
    
    
def plot_all_graphs(df_m1, df_m2, df_m3, v="v1"):
    X = list(range(1, 361))
   
    for df, measure in ((df_m1, "1. measure"), (df_m2, "2. measure"), (df_m3, "3. measure")):
        for winkel in list(df.columns):
            Y = list(df[winkel])
            title = "Measurements for Lenkwinkel {w} Grad from {m}"\
                    .format(w=winkel, m=measure)
            name = fig_path + v + "_" + measure + "_" + winkel + ".png"
            my_plot(X, Y, title, name)
    

def main():
    df_m1 = pd.read_csv(csv_path + "rohdaten_messure_1.csv", index_col=0)
    df_m2 = pd.read_csv(csv_path + "rohdaten_messure_2.csv", index_col=0)
    df_m3 = pd.read_csv(csv_path + "rohdaten_messure_3.csv", index_col=0)
    
    plot_all_graphs(df_m1, df_m2, df_m3, v="v1")


if __name__ == "__main__":
    main()        
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            