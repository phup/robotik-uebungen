#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Dec  6 19:51:43 2018

@author: Phu & Myz
"""
import pandas as pd
import math
import numpy as np
from matplotlib import pyplot as plt
from sklearn import linear_model, datasets
#import cv2 as cv



csv_path = "./Rohdaten/"
L = 0.261     # Der Abstand zwischen dem Vorder- und Hinterrad in cm.


def erstelle_angle_dict():
    df_m1 = pd.read_csv(csv_path + "rohdaten_messure_1.csv", index_col=0)
    df_m2 = pd.read_csv(csv_path + "rohdaten_messure_2.csv", index_col=0)
    df_m3 = pd.read_csv(csv_path + "rohdaten_messure_3.csv", index_col=0)
    
    angle_dict = {}
    keys = list(df_m1.columns)
    for key in keys:
        dist_list = []
        angle_dict[int(key)] = dist_list
        for df in (df_m1, df_m2, df_m3):
            dist_tuple = tuple(df[key])
            dist_list.append(dist_tuple)
            
    return angle_dict






# -----------------------------------> Core function <-----------------------------------------

    # show results
#    print(lines)
#    cv.imshow("Image with lines",img_raw)
#    cv.waitKey(0)
def berechne_linien(angle_dict):
    """
    @author: Phu
    @author2: Myz
    """
    distance_to_center_dict = {}
    for angle, ranges_lists in angle_dict.items():
        distance_to_center_dict[angle] = []


        for ranges_list in ranges_lists:
            x_list = []
            y_list = []
            for single_angle in range(len(ranges_list)):
                if (ranges_list[single_angle] <= 5):

#                    theta = (single_angle *np.pi) / 180
                    theta = ((single_angle + 90) *np.pi) / 180
                    rho = ranges_list[single_angle]
                    x = rho * np.cos(theta)
                    y = rho * np.sin(theta)
                    x_list.append(x)
                    y_list.append(y)

            amount_of_lines = 2
            x_list = np.array(x_list)
            y_list = np.array(y_list)
            # y, X = np.nonzero(bin_img)
            x_list = x_list.reshape(-1, 1)
            y_list = y_list.reshape(-1, 1)
            plt.scatter(x_list, y_list, c='tab:cyan', s=4)

            lines = []
            for i in range(amount_of_lines):
                ransac = linear_model.RANSACRegressor(residual_threshold=0.1)
                ransac.fit(x_list, y_list)
                inlier_mask = ransac.inlier_mask_
#                outlier_mask = np.logical_not(inlier_mask)

                line_X = np.arange(x_list.min(), x_list.max())[:, np.newaxis]
                line_y_ransac = ransac.predict(line_X)

                x_list = np.delete(x_list, np.where(inlier_mask), 0)
                y_list = np.delete(y_list, np.where(inlier_mask), 0)

                # Calculation of line equation
                points = [(line_X[0][0], line_y_ransac[0][0]), (line_X[-1][0], line_y_ransac[-1][0])]
                x_coords, y_coords = zip(*points)
                A = np.vstack([x_coords, np.ones(len(x_coords))]).T
                m, c = np.linalg.lstsq(A, y_coords)[0]
                x = np.linspace(-100., 100.)
                plt.plot(x, m * x + c)
                lines.append((m, c))
                
#                plt.plot(x, (-1/m) * x)  # die Höhengeraden
                
                
                
            line_1 = lines[0]
            line_2 = lines[1]
#            print("line_1:", line_1)
#            print("line_2:", line_2)
            #plt.title(angle)
            plt.xticks(np.arange(-2, 2, 1.0))
            plt.yticks(np.arange(-5, 3, 1.0))

            plt.title(str(angle) +" " +str(line_2[0]) + " " + str(line_1[0]))

            axes = plt.gca()
            axes.set_xlim([-2, 2])
            axes.set_ylim([-5, 3])


            line_1 = lines[0]
            line_2 = lines[1]
            
            a1 = line_1[0]
            b1 = line_1[1]
            a2 = line_2[0]
            b2 = line_2[1]
            
            distance_to_center_line_1 = abs(b1) / np.sqrt(a1 ** 2 + 1)
            distance_to_center_line_2 = abs(b2) / np.sqrt(a2 ** 2 + 1)
            
            # Der Schnittpunkt die Höhe durch (0, 0) zur line_1
            schnittpunkt1 = (((-a1 * b1) / (a1**2 + 1)), ((b1) / (a1**2 + 1)))
            # Der Schnittpunkt die Höhe durch (0, 0) zur line_2
            schnittpunkt2 = (((-a2 * b2) / (a2**2 + 1)), ((b2) / (a2**2 + 1)))
            
            
            
            # ///////////////////////////// core of the core: Zuordnung der Vektoren! /////////////////////////////
            vector1 = (schnittpunkt1[0], schnittpunkt1[1], 0)
            vector2 = (schnittpunkt2[0], schnittpunkt2[1], 0)
            if np.cross(vector1, vector2)[-1] <= 0:
                # tausche distance
                distance_to_center_line_1, distance_to_center_line_2 = tausche(distance_to_center_line_1, distance_to_center_line_2)
                # tausche line
                line_1, line_2 = tausche(line_1, line_2)
                # tausche schnittpunkt
                schnittpunkt1, schnittpunkt2 = tausche(schnittpunkt1, schnittpunkt2)
            
            # plotting für Schnittpunkte
            plt.scatter([schnittpunkt1[0], schnittpunkt2[0], 0], [schnittpunkt1[1], schnittpunkt2[1], 0])
            # plotting für die zwei Höhe-Vektoren
            X_q =  [0, 0]
            Y_q =  [0, 0]
            U_q, V_q = zip(schnittpunkt1, schnittpunkt2)
            C_q = [0, 1]
            plt.quiver(X_q, Y_q, U_q, V_q, C_q, angles='xy', scale_units='xy', scale = 1)
            # /////////////////////////////////////////////////////////////////////////////////////////////////////
            
   
            # Einfügen des (x, y)-Paars!         
            distance_to_center_tuple = (distance_to_center_line_1, distance_to_center_line_2)
            distance_to_center_dict[angle].append(distance_to_center_tuple)



            corner_x = (line_2[1] - line_1[1])/(line_1[0] - line_2[0])
            corner_y = (line_1[0]*line_2[1] - line_1[1]*line_2[0]) / (line_1[0] - line_2[0])
#            print(corner_x,corner_y)
            
            plt.plot()
            plt.scatter([corner_x], [corner_y], c='r', s=100)
#            corner = (corner_x, corner_y)
#            distance_to_center_dict[angle].append(corner)

#            plt.savefig(angle +" " +str(line_2[0]) + " " + str(line_1[0]) + ".png")
            plt.show()



            print(distance_to_center_line_1)
            print(distance_to_center_line_2)
   
    return distance_to_center_dict
#    print(distance_to_center_dict)

# ----------------------------------------------------------------------------------------------------


def tausche(x, y):
    return y, x




# Main
#angle_dict = erstelle_angle_dict()
#distance_to_center_dict = berechne_linien(angle_dict)
#print(distance_to_center_dict)













#
## Verältet!
#def berechne_dist(angle_dict):
#    """
#    Berechne die Koordinaten (x1,y1), (x2,y2) und (x3, y3) jeweils
#    für Winkel von 0 bis 179.
#    Das Ergebnis wird in dist_dic gespeichert
#    """
#    dist_dic = {}
#
#    for angle, ranges_lists in angle_dict.items():
#        distances = []
#        dist_dic[angle] = distances
#        for ranges_list in ranges_lists:
#            # ranges_list = np.asarray(ranges_list)
#            left_wall_distance = min(ranges_list[45:136])
#            front_wall_distance = min(ranges_list[0:46] + ranges_list[315:361])
#            distances.append((left_wall_distance,front_wall_distance))    
#    
#    return dist_dic



def berechne_mittelpunkt(p1, p2, p3):
    """
    Berechne den Mittelpunkt mittels 
    p1 = (x1, y1)
    p2 = (x2, y2)
    p3 = (x3, y3)
    """
    (x1, y1) = p1
    (x2, y2) = p2
    (x3, y3) = p3
    
    A = [[2*(x2-x1), 2*(y2-y1)], [2*(x3-x1), 2*(y3-y1)]]
    b = [(x2**2-x1**2+y2**2-y1**2), (x3**2-x1**2+y3**2-y1**2)]
    
    p0 = np.linalg.solve(A, b)
    
    return p0



def berechne_radius(p0, p1):
    """
    Berechne das Radius mittels
    p0 = (x0, y0)   # also der Mittelpunkt
    p1 = (x1, y1)
    """
    (x0, y0) = p0
    (x1, y1) = p1
    r = math.sqrt((x1 - x0)**2 + (y1 - y0)**2)
    return r


def berechne_alle_mittelpunkte(dist_dic):
    centre_dic = {}
    for key in dist_dic.keys():
        (p1, p2, p3) = dist_dic[key]
        centre = berechne_mittelpunkt(p1, p2, p3)
        centre_dic[key] = list(centre)
        
    return centre_dic


def berechne_alle_radien(dist_dic):
    centre_dic = berechne_alle_mittelpunkte(dist_dic)
    radien_dic = {}
    
    for key in dist_dic.keys():
        p0 = centre_dic[key]
        p1 = dist_dic[key][0]
        r = berechne_radius(p0, p1)
        radien_dic[key] = r
        
    return radien_dic


def berechne_winkel(r):
    gamma = np.arctan(L/r)
    gamma = gamma * 180./np.pi
    
    return gamma


def berechne_alle_winkel(radien_dic):
    winkel_dic = {}
    
    for key in radien_dic.keys():
        gamma = berechne_winkel(radien_dic[key])
        winkel_dic[key] = gamma
        
    return winkel_dic



def main():
    angle_dict = erstelle_angle_dict()
    dist_dic = berechne_linien(angle_dict)
#    print(dist_dic)
    radien_dic = berechne_alle_radien(dist_dic)
    winkel_dic = berechne_alle_winkel(radien_dic)
    
    return dist_dic, radien_dic, winkel_dic




if __name__ == "__main__":
    dist_dic, radien_dic, winkel_dic = main()
    
    print("\n* dist_dic:")
    print(dist_dic)
    print("\n* radien_dic:")
    print(radien_dic)
    print("\n* winkel_dic:")
    print(winkel_dic)
