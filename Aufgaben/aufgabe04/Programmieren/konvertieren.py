#!/usr/bin/env python
# coding: utf8


"""
author: Myz
time: 05:36 21.11.2018
"""

import numpy as np
import cv2 as cv
import time

THRESHOLD = 245


image_raw = "./image_raw.png"
image_path = "./images/"

def set_threshold(img, threshold):
    height, width = img.shape                   # (480, 640) as default
    img_bw = img

    for i in range(height):                     # Zeilenindex
         for j in range(width):                 # Spaltenindex
             if img_bw[i][j] >= threshold:      # threshold = 200 z.B.
                 img_bw[i][j] = 999             # set white
             else:
                 img_bw[i][j] = 0               # set black

    return img_bw

def manuel_sift(img):
    """
    245 ist schon das richtige Threshold in unserem
    Fall. Dennoch sind die zwei Lampen der zwei anderen
    Modelcars noch manuell auszusieben.
    """
    h, w = img.shape

    for i in range(h):
        for j in range(w):
            if i < 0.45 * h:                    # die vordere Lampe aussieben
                img[i][j] = 0
            if j < 0.15 * w:                    # die linke Lampe aussieben
                img[i][j] = 0

    return img



def main(threshold):
    img = cv.imread(image_raw, 0)
    img_bw = set_threshold(img, threshold)
    img_bw = manuel_sift(img_bw)

    t = int(time.time())                # timestamp
    cv.imwrite(image_path + "{}_image_bw_v2_{}.png".\
            format(threshold, str(t)), img_bw)


if __name__ == "__main__":
    main(THRESHOLD)











