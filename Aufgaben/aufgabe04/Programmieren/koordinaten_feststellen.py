#!/usr/bin/env python
# coding: utf8

"""
author: Myz
time: 06:16 21.11.2018

 Sicherlich ist es bei Aufgabe 4 höhere Klassifikations-
 algorithemn zu verwenden, um die 6 Zentren zu finden,
 z.B. mit k-Means. 
 Dennoch verwenden wir einfach den folgenden einfachen
 Algorithmus, da es in unserem Fall recht simpel ist.
     1. Finde die 6 Blöcke mit weiß gefärbten Punkten.
     2. Berechne den jeweiligen Mittelpunkt der Blöcke.
"""

#import numpy as np
import cv2 as cv
import time

image_path = "./images/"
image_name = "MIN240_MAX270_image_bw_v5_1542847095.png"


def finde_die_bloecke(img):
    h, w = img.shape
   
    # initialsiere block_dic
    block_dic = {}
    for i in range(1, 7):
        block_dic[i] = []

    # weiße Punkte jeweiligem block_dic[i] zuordnen
    key = 1
    for i in range(1, h):
        for j in range(1, w):
            if img[i][j] > 0:
                if img[i-1][j] > 0:          # der obere benachbarte Vorgänger ist auch weiß gefärbt.
                    for k in block_dic.keys():
                        if (i-1, j) in block_dic[k]:
                            block_dic[k].append((i, j))
                            break
                elif img[i][j-1] > 0:        # der linke benachbarte Vorgänger ist auch weiß gefärbt.
                    for k in block_dic.keys():
                        if (i, j-1) in block_dic[k]:
                            block_dic[k].append((i, j))
                            break
                elif img[i-1][j-1] > 0:        # der oben-linke benachbarte Vorgänger ist auch weiß gefärbt.
                    for k in block_dic.keys():
                        if (i-1, j-1) in block_dic[k]:
                            block_dic[k].append((i, j))
                            break
                elif img[i-1][j+1] > 0:        # der oben-rechte benachbarte Vorgänger ist auch weiß gefärbt.
                    for k in block_dic.keys():
                        if (i-1, j+1) in block_dic[k]:
                            block_dic[k].append((i, j))
                            break
                else:                   
                    if key > 6:              # dem nächsten Block zuordnen
#                        continue
                        print("Key is larger than 6!")      # kommt trotzdem dreimal vor...
                    else:
                        block_dic[key].append((i, j))
                        key += 1

    return block_dic


def finde_die_zentren(block_dic):
    """
    Bemerke:
    x entspricht dem Spaltenindex, sprich koor[1].
    y entspricht dem Zeilenindex, sprich koor[0].
    """
    ans_dic = {}

    for k in block_dic.keys():
        N = len(block_dic[k])
        x_summe = 0
        y_summe = 0
        for koor in block_dic[k]:
            y_summe += koor[0]              # alle y aufsummieren
            x_summe += koor[1]              # alle y aufsummieren
        x = float(x_summe)/N
        y = float(y_summe)/N
        ans_dic[k] = (int(x), int(y))                 # abrunden

    ans = ans_dic.values()
    
    return ans


def faerbe_die_zentren_schwarz(img, zentren):
    zentren = zentren
    h, w = img.shape

    for y in range(h):
        for x in range(w):
            if (x, y) in zentren:
                img[y][x] = 0            # Zentren schwarz einfärben

    return img



def main():
    img = cv.imread(image_path+image_name, 0)
    block_dic = finde_die_bloecke(img)
    zentren = finde_die_zentren(block_dic)
    print("Die sechs gefundenen Zentren:")
    print(zentren)

    img_neu = faerbe_die_zentren_schwarz(img, zentren)
    t = int(time.time())                # timestamp
    cv.imwrite(image_path + "image_bw_v5_{}.png".\
            format(str(t)), img_neu)



if __name__ == "__main__":
    main()



