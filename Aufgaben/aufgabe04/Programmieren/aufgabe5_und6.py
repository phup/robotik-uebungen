#!/usr/bin/env python
# coding: utf8

"""
author: Myz, Phu
time: 06:16 21.11.2018
"""

# Sicherlich ist es bei Aufgabe 4 höhere Klassifikations-
# algorithemn zu verwenden, um die 6 Zentren zu finden,
# z.B. mit k-Means. 
# Dennoch verwenden wir einfach den folgenden einfachen
# Algorithmus, da es in unserem Fall recht simpel ist.
# 1. Finde die 6 Blöcke mit weiß gefärbten Punkten.
# 2. Berechne den jeweiligen Mittelpunkt der Blöcke.


import numpy as np
import cv2 as cv
import time

image_path = "./images/"
image_name = "245_image_bw_v2_1542776837.png"


def finde_die_bloecke(img):
    h, w = img.shape
   
    # initialsiere block_dic
    block_dic = {}
    for i in range(1, 7):
        block_dic[i] = []

    # weiße Punkte jeweiligem block_dic[i] zuordnen
    key = 1
    for i in range(1, h):
        for j in range(1, w):
            if img[i][j] > 0:
                if img[i-1][j] > 0:          # der obere benachbarte Vorgänger ist auch weiß gefärbt.
                    for k in block_dic.keys():
                        if (i-1, j) in block_dic[k]:
                            block_dic[k].append((i, j))
                            break
                elif img[i][j-1] > 0:        # der linke benachbarte Vorgänger ist auch weiß gefärbt.
                    for k in block_dic.keys():
                        if (i, j-1) in block_dic[k]:
                            block_dic[k].append((i, j))
                            break
                elif img[i-1][j-1] > 0:      # der oben-linke benachbarte Vorgänger ist auch weiß gefärbt.
                    for k in block_dic.keys():
                        if (i-1, j-1) in block_dic[k]:
                            block_dic[k].append((i, j))
                            break
                else:                   
                    if key > 6:              # dem nächsten Block zuordnen
                        continue
    #                    print("Key is larger than 6!")      # kommt trotzdem dreimal vor...
                    else:
                        block_dic[key].append((i, j))
                        key += 1

    return block_dic


def finde_die_zentren(block_dic):
    """
    Bemerke:
    x entspricht dem Spaltenindex, sprich koor[1].
    y entspricht dem Zeilenindex, sprich koor[0].
    """
    ans_dic = {}
    ans_array = []

    for k in block_dic.keys():
        N = len(block_dic[k])
        x_summe = 0
        y_summe = 0
        for koor in block_dic[k]:
            y_summe += koor[0]              # alle y aufsummieren
            x_summe += koor[1]              # alle y aufsummieren
        x = float(x_summe)/N
        y = float(y_summe)/N
        ans_dic[k] = (int(x), int(y))                 # abrunden
        ans_array.append((int(x), int(y)))

    print(ans_array)
    return ans_array


def faerbe_die_zentren_schwarz(img, koor_dic):
    zentren = koor_dic.values()
    h, w = img.shape

    for y in range(h):
        for x in range(w):
            if (x, y) in zentren:
                img[y][x] = 0            # Zentren schwarz einfärben

    return img


def solvePNP(img_points):
    img_points = np.flip((np.float32(np.array(img_points))),axis= 0)
    """
    Die Punkte sind folgendermaßen sortiert:    [5,6]
                                                [3,4]
                                                [1.2]
                                            
    """
    fx = 614.1699
    fy = 614.9002
    cx = 329.9491
    cy = 237.2788
    camera_mat = np.zeros((3, 3, 1))
    camera_mat[:, :, 0] = np.array([[fx, 0, cx],
                                    [0, fy, cy],
                                    [0, 0, 1]])
    k1 = 0.1115
    k2 = -0.1089
    p1 = 0
    p2 = 0
    dist_coeffs = np.zeros((4, 1))
    dist_coeffs[:, 0] = np.array([[k1, k2, p1, p2]])
    # far to close, left to right (order of discovery) in cm
    obj_points = np.zeros((6, 3, 1))
    obj_points[:, :, 0] = np.array([[00.0, 00.0, 0],
                                    [21.8, 00.0, 0],
                                    [00.0, 30.0, 0],
                                    [22.2, 30.0, 0],
                                    [00.0, 60.0, 0],
                                    [22.0, 60.0, 0]])
    retval, rvec, tvec = cv.solvePnP(obj_points, img_points, camera_mat, dist_coeffs)
    rmat = np.zeros((3, 3))

    cv.Rodrigues(rvec, rmat, jacobian=0)

    homo_matrix = np.float32(np.concatenate((rmat,tvec),axis= 1))
    vector = np.transpose(np.array([0,0,0,1]))

    homo_matrix = np.vstack((homo_matrix,vector))
    inv_homo_matrix = np.linalg.inv(homo_matrix)

    # calculation of euler angles : https://stackoverflow.com/questions/15022630/how-to-calculate-the-angle-from-rotation-matrix
    euler1 = np.arctan2(inv_homo_matrix[2][1],inv_homo_matrix[2][2])
    euler2 = np.arctan2(-inv_homo_matrix[2][0],np.sqrt(np.square(inv_homo_matrix[2][1]) +  np.square(inv_homo_matrix[2][2])))
    euler3 = np.arctan2(inv_homo_matrix[1][0],inv_homo_matrix[0][0])

    print(euler1, euler2, euler3)
    # -1.6741036470100992 0.060273758287017996 -0.041510819686257826
    # -96 Grad, 3,45 Grad, -2,37 Grad



    # print(np.dot(homo_matrix,np.array([421., 240, 0., 1.])))





def main():
    img = cv.imread(image_path+image_name, 0)
    block_dic = finde_die_bloecke(img)
    koord_array = finde_die_zentren(block_dic)
    print(koord_array)
    solvePNP(koord_array)

    print("Die sechs gefundenen Zentren:")
    print(koord_array)

    # img_neu = faerbe_die_zentren_schwarz(img, koord_array)
    # t = int(time.time())                # timestamp
    #
    # cv.imshow("window", img_neu)
    # cv.waitKey(0)

    # cv.imwrite(image_path + "image_bw_v3_{}.png".\
    #         format(str(t)), img_neu)



if __name__ == "__main__":
    main()



