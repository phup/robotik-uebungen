#!/usr/bin/env python

# --- imports ---
import rospy
import sensor_msgs.msg
from operator import itemgetter
from myz_steering_calibration.srv import *
from std_msgs.msg import Float32
from std_msgs.msg import String
from nav_msgs.msg import Odometry
from math import sqrt
from std_msgs.msg import UInt8
from std_msgs.msg import Int16
# --- definitions ---
last_driving_control_info = ""
epsilon = 0.05   # allowed inaccuracy for distance calculation
speed = 200
angle_left = 0
angle_straight = 86
angle_right = 180
last_odom = None
is_active = False
all_angles = [0,30,60,90,120,150,179]
distance = 0.2

def callbackOdom(msg):
    global last_odom
    last_odom = msg
def waitForFirstOdom():
    while not rospy.is_shutdown() and last_odom is None:
        rospy.loginfo(
            "%s: No initial odometry message received. Waiting for message...",
            rospy.get_caller_id())
        rospy.sleep(1.0)

def callbackDrivingControl(msg):
    last_driving_control_info = msg.data
"""
def callbackScanData(data):
    yaw_message = rospy.get_caller_id() + " I heard " + str(data.ranges)
    # publisher.publish(yaw_message)
    maxi = max(enumerate([x for x in data.ranges if x!= float('inf')]), key=itemgetter(1))[0] 
    
    rospy.sleep(5)	
    print(maxi)
    return 0
"""
def callbackBackwardLongitudinal(request):
    rospy.loginfo(rospy.get_caller_id() + ": callbackBackwardLongitudinal, angle = " + str(request.angle) + "speed= " + str(request.speed) )
    
    angle_ranges_dict = {}

    for angle in all_angles:
        msg = rospy.wait_for_message('/scan', sensor_msgs.msg.LaserScan)
        currentRanges = msg.ranges
        angle_ranges_dict[str(angle)] = [msg.ranges] 
        for counter in range(2):

            drive(distance, "drive", speed, angle)
            rospy.sleep(5)
            msg = rospy.wait_for_message('/scan', sensor_msgs.msg.LaserScan)
            currentRanges = msg.ranges
            angle_ranges_dict[str(angle)].append(msg.ranges)
        drive(distance, "drive", speed*-1, angle)
        drive(distance, "drive", speed*-1, angle)

    print(angle_ranges_dict)
    

    fout = "/root/angles.txt"
    fo = open(fout, "w")

    for k, v in angle_ranges_dict.items():
        fo.write(str(k) + ' >>> '+ str(v) + '\n\n')

    fo.close()


    return ParkingManeuverResponse("FINISHED")

def drive(distance, command, speed, angle):
    global is_active

    rospy.loginfo("%s: Running %s(%f)", rospy.get_caller_id(), command, distance)
    if distance <= 0:
        rospy.logerr(
            "%s: Error, distance argument has to be > 0! %f given",
            rospy.get_caller_id(),
            distance)
        return

    pub_info.publish("BUSY")
    if is_active:
        rospy.logwarn(
            "%s: Warning, another command is still active! Please wait and try again.",
            rospy.get_caller_id())
        return

    is_active = True

    # stop the car and set desired steering angle + speed
    pub_speed.publish(0)
    pub_stop_start.publish(1)
    rospy.sleep(1)
    pub_steering.publish(angle)
    pub_stop_start.publish(0)
    rospy.sleep(1)
    pub_speed.publish(speed)

    start_pos = last_odom.pose.pose.position
    current_distance = 0

    while not rospy.is_shutdown() and current_distance < (distance - epsilon):
        current_pos = last_odom.pose.pose.position
        current_distance = sqrt(
            (current_pos.x - start_pos.x)**2 + (current_pos.y - start_pos.y)**2)
        # rospy.loginfo("current distance = %f", current_distance)
        rospy.sleep(0.1)

    pub_speed.publish(0)
    is_active = False
    current_pos = last_odom.pose.pose.position
    current_distance = sqrt((current_pos.x - start_pos.x)
                            ** 2 + (current_pos.y - start_pos.y)**2)
    pub_info.publish("FINISHED")

    rospy.loginfo(
        "%s: Finished %s(%f)\nActual travelled distance = %f",
        rospy.get_caller_id(),
        command,
        distance,
        current_distance)


# --- main program ---

# In ROS, nodes are uniquely named. If two nodes with the same
# node are launched, the previous one is kicked off. The
# anonymous=True flag means that rospy will choose a unique
# name for our node so that multiple instances can
# run simultaneously.
rospy.init_node("simple_parking_maneuver")
sub_backward_longitudinal = rospy.Service(
    "simple_parking_maneuver/backward_longitudinal",
    ParkingManeuver,
    callbackBackwardLongitudinal)

if rospy.has_param("speed_rpm"):
    speed_rpm = rospy.get_param("speed_rpm")

# create subscribers and publishers
sub_odom = rospy.Subscriber("odom", Odometry, callbackOdom, queue_size=100)
# wait for first odometry message, till adverting subscription of commands
waitForFirstOdom()


pub_stop_start = rospy.Publisher(
    "manual_control/stop_start",
    Int16,
    queue_size=100)
pub_speed = rospy.Publisher("manual_control/speed", Int16, queue_size=100)
pub_steering = rospy.Publisher(
    "steering",
    UInt8,
    queue_size=100)
pub_info = rospy.Publisher("simple_drive_control/info", String, queue_size=100)
# sub_scan = rospy.Subscriber("/scan", sensor_msgs.msg.LaserScan, callbackScanData)
rospy.loginfo(rospy.get_caller_id() + ": started!")

# spin() simply keeps python from exiting until this node is stopped
rospy.spin()

