#!/usr/bin/env python

import rospy
import numpy as np
from nav_msgs.msg import Odometry
from std_msgs.msg import Int16
from std_msgs.msg import UInt8
from tf.transformations import euler_from_quaternion

Mittelpunkt_K1 = (195, 215)
Mittelpunkt_K2 = (405, 215)

Radius_inner_circle = 135
Radius_outer_circle = 165
H_G1_L1 = 80
H_G2_L1 = 350

H_G1_L2 = 50
H_G2_L2 = 380

X_MIN, X_MAX = 0, 600
Y_MIN, Y_MAX = 0, 430
linke_grenze, rechte_grenze, mittle_grenze = 185, 415, 215

pub_speed = rospy.Publisher("/target_speed", Int16, queue_size=100)
pub_lane = rospy.Publisher("/lane_change", UInt8, queue_size=100)


def callback_scan_data(self, data):
    position_msg = rospy.wait_for_message('/localization/odom/11', Odometry)
    car_x = position_msg.pose.pose.position.x
    car_y = position_msg.pose.pose.position.y
    z = position_msg.pose.pose.orientation.z
    w = position_msg.pose.pose.orientation.w
    roll, pitch, yaw = euler_from_quaternion((0, 0, z, w))
    ranges = data.ranges

    closest_car_point = find_closest_point((car_x * 100, car_y * 100), self.laneID, self.lookDistance)
    closest_car_point = (closest_car_point[0] / 100, closest_car_point[1] / 100)

    # print(ranges[45:136])
    angle_dict = {}
    min_distance_l1 = 100.
    min_distance_l2 = 100.
    min_point_l1 = (9000., 9000.)
    min_point_l2 = (9000., 9000.)
    distance_to_obstacle_l1 = 100.
    distance_to_obstacle_l2 = 100.

    # print(ranges[270])
    ranges = list(ranges)  # .reverse()
    ranges.reverse()
    for i in range(len(ranges)):
        if ranges[i] <= 1.5 and ((i >= 0 and i <= 45) or (i >= 315 and i <= 360)):
            angle_dict[str(i)] = ranges[i]
    # print(angle_dict)
    # print(ranges[270])
    for angle, magnitude in angle_dict.items():

        angle_rad = -1 * np.pi + float(angle) * np.pi / 180
        # print(angle_rad)
        obstacle_pointx, obstacle_pointy = (magnitude * np.sin(angle_rad), magnitude * np.cos(angle_rad))
        o_w = calc_world_coord(obstacle_pointx, obstacle_pointy, car_x, car_y, yaw)
        o_w = np.array((o_w[0], o_w[1]))
        o_w_cm = np.array((o_w[0] * 100, o_w[1] * 100))
        # print(o_w_cm)
        closest_point_lane1 = find_closest_point(o_w_cm, 1, 0)
        closest_point_lane2 = find_closest_point(o_w_cm, 2, 0)

        closest_point_lane1 = np.array((closest_point_lane1[0] / 100., closest_point_lane1[1] / 100.))
        closest_point_lane2 = np.array((closest_point_lane2[0] / 100., closest_point_lane2[1] / 100.))

        distance_to_closest_point_l1 = np.linalg.norm(o_w - closest_point_lane1)
        distance_to_closest_point_l2 = np.linalg.norm(o_w - closest_point_lane2)
        # if distance_to_closest_point_l1 < 0.15 and distance_to_closest_point_l2 < 0.15:
        # print(distance_to_closest_point_l1,distance_to_closest_point_l2)
        if distance_to_closest_point_l1 < 0.1:
            distance_to_obstacle_l1 = np.linalg.norm(np.array(closest_car_point) - np.array(closest_point_lane1))
            if distance_to_obstacle_l1 < min_distance_l1:
                min_distance_l1 = distance_to_obstacle_l1
                min_point_l1 = closest_point_lane1
        if distance_to_closest_point_l2 < 0.1:
            distance_to_obstacle_l2 = np.linalg.norm(np.array(closest_car_point) - np.array(closest_point_lane2))
            if distance_to_obstacle_l2 < min_distance_l2:
                min_distance_l2 = distance_to_obstacle_l2
                min_point_l2 = closest_point_lane2

    print(min_distance_l1, min_distance_l2)
    # closest_car_point = find_closest_point((car_x * 100, car_y * 100), self.laneID, self.lookDistance)
    # closest_car_point= (closest_car_point[0]/100,closest_car_point[1]/100)
    # distance_to_obstacle_l1 = np.linalg.norm(np.array(closest_car_point) - np.array(min_point_l1))
    # distance_to_obstacle_l2 = np.linalg.norm(np.array(closest_car_point) - np.array(min_point_l2))
    # print(min_point_l1,min_point_l2)
    # print(distance_to_obstacle_l1,distance_to_obstacle_l2)
    if distance_to_obstacle_l1 <= 0.7 and distance_to_obstacle_l1 <= 0.7:
        pub_speed.publish(0)
        print("stop")
    elif distance_to_obstacle_l1 <= 0.7:
        pub_lane.publish(2)
        print("change2lane2")
    elif distance_to_obstacle_l2 <= 0.7:
        pub_lane.publish(1)
        print("change2lane1")


def calc_world_coord(o_x, o_y, c_x, c_y, car_orientation):
    cos = np.cos(car_orientation)
    sin = np.sin(car_orientation)
    transformation_matrix = np.array(
        [[cos, -1. * sin, 0., c_x], [sin, cos, 0., c_y], [0., 0., 1., 0.], [0., 0., 0., 1.]])
    o_w = np.dot(transformation_matrix, np.array([o_x, o_y, 0, 1]))
    return o_w

def closest_point_to_a_line(punkt, h):
    """
    Die gegebene Gerade: y = h
    """
    x, y = punkt
    P_x = x
    return (P_x, h)


def closest_point_to_a_halfcircle(punkt, mittelpunkt, r):
    """
    (M_x, M_y) ist der Mittelpunkt des Kreises, und r ist der Radius
    """
    x, y = punkt
    M_x, M_y = mittelpunkt

    d = np.sqrt((x - M_x) ** 2 + (y - M_y) ** 2)
    P_x = (r * (float(x - M_x) / d) + M_x)
    P_y = (r * (float(y - M_y) / d) + M_y)

    return (P_x, P_y)


def calc_closest_point_with_distance_for_circles(punkt, lane, distance, circle_ID):
    if lane == 1:
        radius = Radius_inner_circle
    if lane == 2:
        radius = Radius_outer_circle

    if circle_ID == 1:
        center = Mittelpunkt_K1
        ansx, ansy = closest_point_to_a_halfcircle(punkt, center, radius)
        # print("test")
        # print(ansx,ansy,center[1])
        # print("test")
        # print(radius)
        # print(distance)
        a = np.arccos((center[1] - ansy) / radius) - float(distance) / radius
        # print(a)

        # print(center[1]-ansy)

        ansx = center[0] - np.sin(a) * radius
        ansy = center[1] - np.cos(a) * radius

        # print(punkt,lane,distance,circle_ID)

        if ansx > linke_grenze:
            circle_distance = np.arccos((center[1] - ansy) / radius) * radius

            if lane == 1:
                height = H_G1_L1
            if lane == 2:
                height = H_G1_L2
            ans = calc_closest_point_with_distance_for_line((linke_grenze, height), lane, distance - circle_distance, 1)

            return (int(ans[0]), int(ans[1]))

    if circle_ID == 2:
        center = Mittelpunkt_K2
        ansx, ansy = closest_point_to_a_halfcircle(punkt, center, radius)
        a = np.arccos((center[1] - ansy) / radius) + float(distance) / radius
        ansx = center[0] + np.sin(a) * radius
        ansy = center[1] - np.cos(a) * radius

        if ansx < rechte_grenze:
            circle_distance = (np.pi - np.arccos((center[1] - ansy) / radius)) * radius

            if lane == 1:
                height = H_G2_L1
            if lane == 2:
                height = H_G2_L2
            ans = calc_closest_point_with_distance_for_line((rechte_grenze, height), lane, distance - circle_distance,
                                                            2)

            return (int(ans[0]), int(ans[1]))

    return (int(ansx), int(ansy))


def calc_closest_point_with_distance_for_line(punkt, lane, distance, line_ID):
    if line_ID == 1:
        if lane == 1:
            line_height = H_G1_L1
        if lane == 2:
            line_height = H_G1_L2
        ans = closest_point_to_a_line(punkt, line_height)
        ans = (ans[0] + distance, ans[1])
        if ans[0] > rechte_grenze:
            ans = calc_closest_point_with_distance_for_circles((rechte_grenze, line_height), lane,
                                                               ans[0] - rechte_grenze, 2)

    if line_ID == 2:
        if lane == 1:
            line_height = H_G2_L1

        if lane == 2:
            line_height = H_G2_L2
        ans = closest_point_to_a_line(punkt, line_height)
        ans = (ans[0] - distance, ans[1])

        if ans[0] < linke_grenze:
            ans = calc_closest_point_with_distance_for_circles((linke_grenze, line_height), lane, linke_grenze - ans[0],
                                                               1)

    return ans



def find_closest_point(punkt, lane, distance):
    x, y = punkt

    # Ueberpruefe punkt zunaechst, ob er gueltig ist
    # print(x,y)
    if x < X_MIN or x > X_MAX:
        # print("Ungueltiges x!")
        return (9000, 9000)

    if y < Y_MIN or y > Y_MAX:
        # print("Ungueltiges y!")
        return (9000, 9000)

    # Berechne den zutreffenden Punkt je nach dem vier Bereichen
    if x < linke_grenze:
        ans = calc_closest_point_with_distance_for_circles(punkt, lane, distance, 1)
        return ans
    elif x > rechte_grenze:
        ans = calc_closest_point_with_distance_for_circles(punkt, lane, distance, 2)
        return ans

    elif y < mittle_grenze:
        ans = calc_closest_point_with_distance_for_line(punkt, lane, distance, 1)
        return ans
    else:
        ans = calc_closest_point_with_distance_for_line(punkt, lane, distance, 2)

    return ans



try:
    rospy.spin()
except KeyboardInterrupt:
    print("Shutting down")