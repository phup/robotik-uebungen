import numpy as np
import rospy
from std_msgs.msg import UInt8
from tf.transformations import euler_from_quaternion


Mittelpunkt_K1 = (195, 215)
Mittelpunkt_K2 = (405, 215)

Radius_inner_circle = 135
Radius_outer_circle = 165
H_G1_L1 = 80
H_G2_L1 = 350

H_G1_L2 = 50
H_G2_L2 = 380

X_MIN, X_MAX = 0, 600
Y_MIN, Y_MAX = 0, 430
linke_grenze, rechte_grenze, mittle_grenze = 185, 415, 215


def position_callback(self, msg):
    # pub_speed.publish(180)

    x = msg.pose.pose.position.x
    y = msg.pose.pose.position.y
    z = msg.pose.pose.orientation.z
    w = msg.pose.pose.orientation.w
    roll, pitch, yaw = euler_from_quaternion((0, 0, z, w))

    xcm = x * 100
    ycm = y * 100
    # print(xcm,ycm)
    # print(find_closest_point((69,281),1,50))
    closest_point = np.array(find_closest_point((xcm, ycm), self.laneID, self.lookDistance))
    # print(xcm,ycm)
    # print(closest_point)
    numpy_point = np.array((xcm, ycm))

    closest_point_vector = closest_point - numpy_point
    vector = np.array((1, 0))
    # print(closest_point_vector)
    # print((closest_point_vector))
    # print(vector)
    closest_point_error = np.arccos(
        np.dot(vector, closest_point_vector) / (np.linalg.norm(vector) * np.linalg.norm(closest_point_vector)))

    # print(yaw,closest_point_error)
    if closest_point_vector[1] >= 0:
        closest_point_error = closest_point_error * -1  # + np.pi
    else:
        closest_point_error = closest_point_error  # + np.pi
    error = closest_point_error + yaw
    if error > np.pi:
        error -= 2 * np.pi
    elif error < -1 * np.pi:
        error += 2 * np.pi
    # print(closest_point_vector)
    # print(-1*closest_point_vector[1])
    # print(closest_point_error,yaw)
    # print (error)
    # error = (error/np.pi)*180 + 86
    # print(error)
    # print(error)
    self.previous_angle = error

    steering_angle = self.angle_p_weight * (error) + self.angle_d_weight * (error - self.previous_angle)
    steering_angle = steering_angle / np.pi * 180 + 86
    self.all_angles.append(steering_angle)
    # print(steering_angle)
    self.change_counter += 1
    if self.change_counter == 10:
        mean_error = np.median(self.all_angles)
        current_angle = mean_error
        if current_angle > 180:
            current_angle = 180
        if current_angle < 0:
            current_angle = 0
        self.change_counter = 0
        self.all_angles = []
        # print(current_angle)
        pub_steering.publish(current_angle)


def lane_hange



def closest_point_to_a_line(punkt, h):
    """
    Die gegebene Gerade: y = h
    """
    x, y = punkt
    P_x = x
    return (P_x, h)


def closest_point_to_a_halfcircle(punkt, mittelpunkt, r):
    """
    (M_x, M_y) ist der Mittelpunkt des Kreises, und r ist der Radius
    """
    x, y = punkt
    M_x, M_y = mittelpunkt

    d = np.sqrt((x - M_x) ** 2 + (y - M_y) ** 2)
    P_x = (r * (float(x - M_x) / d) + M_x)
    P_y = (r * (float(y - M_y) / d) + M_y)

    return (P_x, P_y)


def calc_closest_point_with_distance_for_circles(punkt, lane, distance, circle_ID):
    if lane == 1:
        radius = Radius_inner_circle
    if lane == 2:
        radius = Radius_outer_circle

    if circle_ID == 1:
        center = Mittelpunkt_K1
        ansx, ansy = closest_point_to_a_halfcircle(punkt, center, radius)
        # print("test")
        # print(ansx,ansy,center[1])
        # print("test")
        # print(radius)
        # print(distance)
        a = np.arccos((center[1] - ansy) / radius) - float(distance) / radius
        # print(a)

        # print(center[1]-ansy)

        ansx = center[0] - np.sin(a) * radius
        ansy = center[1] - np.cos(a) * radius

        # print(punkt,lane,distance,circle_ID)

        if ansx > linke_grenze:
            circle_distance = np.arccos((center[1] - ansy) / radius) * radius

            if lane == 1:
                height = H_G1_L1
            if lane == 2:
                height = H_G1_L2
            ans = calc_closest_point_with_distance_for_line((linke_grenze, height), lane, distance - circle_distance, 1)

            return (int(ans[0]), int(ans[1]))

    if circle_ID == 2:
        center = Mittelpunkt_K2
        ansx, ansy = closest_point_to_a_halfcircle(punkt, center, radius)
        a = np.arccos((center[1] - ansy) / radius) + float(distance) / radius
        ansx = center[0] + np.sin(a) * radius
        ansy = center[1] - np.cos(a) * radius

        if ansx < rechte_grenze:
            circle_distance = (np.pi - np.arccos((center[1] - ansy) / radius)) * radius

            if lane == 1:
                height = H_G2_L1
            if lane == 2:
                height = H_G2_L2
            ans = calc_closest_point_with_distance_for_line((rechte_grenze, height), lane, distance - circle_distance,
                                                            2)

            return (int(ans[0]), int(ans[1]))

    return (int(ansx), int(ansy))


def calc_closest_point_with_distance_for_line(punkt, lane, distance, line_ID):
    if line_ID == 1:
        if lane == 1:
            line_height = H_G1_L1
        if lane == 2:
            line_height = H_G1_L2
        ans = closest_point_to_a_line(punkt, line_height)
        ans = (ans[0] + distance, ans[1])
        if ans[0] > rechte_grenze:
            ans = calc_closest_point_with_distance_for_circles((rechte_grenze, line_height), lane,
                                                               ans[0] - rechte_grenze, 2)

    if line_ID == 2:
        if lane == 1:
            line_height = H_G2_L1

        if lane == 2:
            line_height = H_G2_L2
        ans = closest_point_to_a_line(punkt, line_height)
        ans = (ans[0] - distance, ans[1])

        if ans[0] < linke_grenze:
            ans = calc_closest_point_with_distance_for_circles((linke_grenze, line_height), lane, linke_grenze - ans[0],
                                                               1)

    return ans



def find_closest_point(punkt, lane, distance):
    x, y = punkt

    # Ueberpruefe punkt zunaechst, ob er gueltig ist
    # print(x,y)
    if x < X_MIN or x > X_MAX:
        # print("Ungueltiges x!")
        return (9000, 9000)

    if y < Y_MIN or y > Y_MAX:
        # print("Ungueltiges y!")
        return (9000, 9000)

    # Berechne den zutreffenden Punkt je nach dem vier Bereichen
    if x < linke_grenze:
        ans = calc_closest_point_with_distance_for_circles(punkt, lane, distance, 1)
        return ans
    elif x > rechte_grenze:
        ans = calc_closest_point_with_distance_for_circles(punkt, lane, distance, 2)
        return ans

    elif y < mittle_grenze:
        ans = calc_closest_point_with_distance_for_line(punkt, lane, distance, 1)
        return ans
    else:
        ans = calc_closest_point_with_distance_for_line(punkt, lane, distance, 2)

    return ans


pub_steering = rospy.Publisher("steering", UInt8, queue_size=100)
