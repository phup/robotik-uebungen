#! /usr/bin/env python

from global_parameters import *
import rospy
import time
from std_msgs.msg import Int16, UInt8

pub_speed = rospy.Publisher("/manual_control/speed", Int16, queue_size=QUEUE_SIZE) 
pub_steering = rospy.Publisher("/steering", UInt8, queue_size=QUEUE_SIZE)



def stop():
    rospy.init_node('stop_node', anonymous=False)
   # while True:
    rate = rospy.Rate(10)
    num = 0
    while num <= 10:
        pub_speed.publish(0)
        pub_steering.publish(90)
        rospy.loginfo("Modelcar wird gestoppt.")
        num += 1
        rate.sleep()



if __name__ == "__main__":
    try:
        stop()
        rospy.loginfo("**** Modelcar ist gestoppt at {} ****".format(time.ctime()))
    except rospy.ROSInterruptException:
        pass
