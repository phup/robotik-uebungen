#! /usr/bin/env python

from global_parameters import *
import rospy
from std_msgs.msg import Int16, Float64

speed = 40.0          # test

def speed_talker():
    pub = rospy.Publisher('sf_speed_control', Float64, queue_size=QUEUE_SIZE)
    rospy.init_node('speed_control', anonymous=True)
    rate = rospy.Rate(RATE)
    while not rospy.is_shutdown():
        nachricht = "speed: {}".format(str(speed))
        rospy.loginfo(nachricht)
        pub.publish(speed)
        rate.sleep()

if __name__ == '__main__':
    try:
        speed_talker()
    except rospy.ROSInterruptException:
        pass


