#! /usr/bin/env python
# -*- coding: utf-8 -*-

from global_parameters import *
import rospy
from std_msgs.msg import UInt8, Float64, String
import numpy as np

spur = spur1
vorausschau = 0.2

pub = rospy.Publisher('sf_steering_control', Float64, queue_size = QUEUE_SIZE)
pub_point_f = rospy.Publisher('sf_point_f', String, queue_size = QUEUE_SIZE)
pub_point_t = rospy.Publisher('sf_point_t', String, queue_size = QUEUE_SIZE)

class Our_steering(object):
    
    def __init__(self):
        self.tw_x = np.nan
        self.tw_y = np.nan
        self.tw_dir = np.nan
        self.rpm = np.nan
        self.vorausschau = vorausschau
        self.spur = spur            # Auf welcher Spur der Wagen fährt, bzw. fahren wird

        rospy.Subscriber('sf_tw_x', Float64, self.tw_x_callback)
        rospy.Subscriber('sf_tw_y', Float64, self.tw_y_callback)
        rospy.Subscriber('sf_tw_dir', Float64, self.tw_dir_callback)
        rospy.Subscriber('sf_speed_rpm', Float64, self.speed_rpm_callback)

    def tw_x_callback(self, msg):
        self.tw_x = msg.data
        
    def tw_y_callback(self, msg):
        self.tw_y = msg.data
        
    def tw_dir_callback(self, msg):
        self.tw_dir = msg.data

    def speed_rpm_callback(self, msg):
        self.rpm = msg.data
        

def berechne_steering(steer):
    d = steer.vorausschau
    alpha = steer.tw_dir
    P_x = steer.tw_x                    # x Koordinat von Punkt P, wo sich der Wagen befindet
    P_y = steer.tw_y
    Pf_x = P_x + d * np.cos(alpha)     # Punkt Pf entspricht dem vermeintlichen Punkt, der sich in der Fahrrichtung befindet
    Pf_y = P_y + d * np.sin(alpha)
    # Pf steht also für den falschen Punkt, und Pt für den wahren (true) Zielpunkt bei der Vorausschau
    Pt_x, Pt_y = steer.spur.find_closest_point((Pf_x, Pf_y))
    # beta ist der Winkel für die gewünschte Fahrrichtung
    beta = np.arcsin((Pt_y - P_y)/np.sqrt((Pt_x - P_x)**2 + (Pt_y - P_y)**2))   
    # Die Differenz zwischen Wunschfahrrichtung und aktueller Fahrrichtung ergibt sich das Steering, allerdings noch ohne Kalibrierung
    sigma = alpha - beta     
    if sigma > np.pi:
        sigma = sigma - 2*np.pi
    if sigma < -np.pi:
        sigma = sigma + 2*np.pi
    
    Pf_msg = str([Pf_x, Pf_y])
    Pt_msg = str([Pt_x, Pt_y])
    
    rospy.loginfo("sf_steering: {}\n".format(sigma))
    pub.publish(sigma)
    
    rospy.loginfo("point_f: {}\n".format(Pf_msg))
    pub_point_f.publish(Pf_msg)

    rospy.loginfo("point_t: {}\n".format(Pt_msg))
    pub_point_t.publish(Pt_msg)
    


def steering_control_main(steer):
    rospy.init_node('steering_control', anonymous=False)
    rate = rospy.Rate(RATE)
    while not rospy.is_shutdown():
        berechne_steering(steer)
        rate.sleep()

    
if __name__ == "__main__":
    try:
        steer = Our_steering()
        steering_control_main(steer)
#        rospy.spin()
    except rospy.ROSInterruptException:
        pass






