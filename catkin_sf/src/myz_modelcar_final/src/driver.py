#! /usr/bin/env python

from global_parameters import *
import rospy
from std_msgs.msg import Int16, UInt8, Bool, Float64
import time
import os


pub_speed = rospy.Publisher("/manual_control/speed", Int16, queue_size=QUEUE_SIZE)
pub_steering = rospy.Publisher("/steering", UInt8, queue_size=QUEUE_SIZE)

#stop = False

class Our_driver(object):
    
    def __init__(self):
        self.rpm = np.nan
        self.lenkung = np.nan
        
        rospy.Subscriber('sf_speed_control', Float64, self.speed_control_callback)
        rospy.Subscriber('sf_steering_control', Float64, self.steering_control_callback)
        
    
    def speed_control_callback(self, msg):
        auto.rpm = msg.data
        speed_nxt = speed_calibration(auto)
        nachricht = "\n***** speed_nxt nach Kalibrierung: {} *****".format(speed_nxt)
        rospy.loginfo(nachricht)
        
        pub_speed.publish(speed_nxt)

    def steering_control_callback(self, msg):
        auto.lenkung = msg.data
        steering_nxt = steering_calibration(auto)
        nachricht = "\n----- steering_nxt nach Kalibrierung: {} -----".format(steering_nxt)
        rospy.loginfo(nachricht)
        
        pub_steering.publish(steering_nxt)
    



if __name__ == '__main__':
    try:
        rospy.init_node('driver', anonymous=True)
        our_driver = Our_driver()
        rospy.spin()
    except rospy.ROSInternalException:
        pass
    
    
