#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb  9 00:10:15 2019

@author: myz
"""

from gps_datenverarbeitung import positions


L = 0.261


def berechne_mittelpunkt(p1, p2, p3):
    """
    Berechne den Mittelpunkt mittels 
    p1 = (x1, y1)
    p2 = (x2, y2)
    p3 = (x3, y3)
    """
    (x1, y1) = p1
    (x2, y2) = p2
    (x3, y3) = p3
    
    A = [[2*(x2-x1), 2*(y2-y1)], [2*(x3-x1), 2*(y3-y1)]]
    b = [(x2**2-x1**2+y2**2-y1**2), (x3**2-x1**2+y3**2-y1**2)]
    
    p0 = np.linalg.solve(A, b)
    
    return p0



def berechne_radius(p0, p1):
    """
    Berechne das Radius mittels
    p0 = (x0, y0)   # also der Mittelpunkt
    p1 = (x1, y1)
    """
    (x0, y0) = p0
    (x1, y1) = p1
    r = math.sqrt((x1 - x0)**2 + (y1 - y0)**2)
    return r



def berechne_alle_mittelpunkte(dist_dic):
    centre_dic = {}
    for key in dist_dic.keys():
        (p1, p2, p3) = dist_dic[key]
        centre = berechne_mittelpunkt(p1, p2, p3)
        centre_dic[key] = list(centre)
        
    return centre_dic


def berechne_alle_radien(dist_dic):
    centre_dic = berechne_alle_mittelpunkte(dist_dic)
    radien_dic = {}
    
    for key in dist_dic.keys():
        p0 = centre_dic[key]
        p1 = dist_dic[key][0]
        r = berechne_radius(p0, p1)
        radien_dic[key] = r
        
    return radien_dic


def berechne_winkel(r):
    gamma = np.arctan(L/r)
    gamma = gamma * 180./np.pi
    
    return gamma


def berechne_alle_winkel(radien_dic):
    winkel_dic = {}
    
    for key in radien_dic.keys():
        gamma = berechne_winkel(radien_dic[key])
        winkel_dic[key] = gamma
        
    return winkel_dic



def main():
    dist_dic = positions
#    print(dist_dic)
    radien_dic = berechne_alle_radien(dist_dic)
    winkel_dic = berechne_alle_winkel(radien_dic)
    
    return dist_dic, radien_dic, winkel_dic




if __name__ == "__main__":
    dist_dic, radien_dic, winkel_dic = main()
    
    print("\n* dist_dic:")
    print(dist_dic)
    print("\n* radien_dic:")
    print(radien_dic)
    print("\n* winkel_dic:")
    print(winkel_dic)
