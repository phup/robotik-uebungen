#! /usr/bin/env python
# -*- coding: utf-8 -*-

AUTO_ID = 7         #@_@


RATE = 10
QUEUE_SIZE = 10


# ///////// Parameter für das Modelcar ////////
RAD_RADIUS = 0.03      # 3cm
OFFSET = 0.25          # 0.25m ist der Abstand zwischen dem Aruco-Code und dem Mittelpunkt der vorderen Axis




# ///////////// Für Display /////////////


X_MIN, X_MAX = 0, 600
Y_MIN, Y_MAX = 0, 430
Mittelpunkt_K1 = (195, 215)
Mittelpunkt_K2 = (405, 215)
G1 = 96               # Höhe von Gerade 1
G2 = 334              # Höhe von Gerade 2
G13 = 64
G12 = 32
G23 = 366
G22 = 398
LINEWIDTH = 1
LINKE_GRENZE, RECHTE_GRENZE, MITTLE_GRENZE = 195, 405, 215
AUTO_FARBE = 'b'



# redudante:
HERZ = 50
QUEUESIZE = 10
STOP = False
STOP_SPEED = 0
STOP_STEERING = 90
DRIVER_NODE = "/driver_node"
