#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb  8 22:43:49 2019

@author: myz
"""

from matplotlib import pyplot as plt
from global_parameter import *



# die tw_x und tw_y Daten werden in positions gespeichert
positions ={\
            0: ((2.86666179646, 1.48559891659), (2.42754304879, 1.77119203353), (2.09636624203, 1.66169276592)),\
            30: ((2.84942035423, 1.48574950635), (2.68227002336, 1.72148321887), ( 2.11054608549, 1.8037086697)), \
            60: ((2.86070688107, 1.47903790787), (2.68828930546, 1.83029493491), (2.421912069, 2.08022488127)), \
            90: ((2.86511534857, 1.48310586569), (2.83656343399, 1.85853491139), (2.76040035182, 2.15132534472)), \
            120: ((2.84091091516, 1.48314087913), (2.88519469704, 1.80797439111), (2.91987009669, 2.10693208552)), \
            150: ((2.85038467171, 1.48309612907), (3.05710165119, 1.96337708966), (3.25560013875, 2.21253151989)), \
            180: ((2.86554348626, 1.48322912167), (2.99259274754, 1.69485796876), (3.32551242214, 1.93657451717)), \
            }


def plotting(positions):
    i = 0
    farben = ['black', 'brown', 'grey', 'blue', 'green', 'red', 'yellow']
    pos = list(positions.values())
    for bahn in pos:
        farbe = farben[i]
        for punkt in bahn:
            x, y = punkt
            x *= 100
            y *= 100
            plt.scatter(x, y, color = farbe, s=5)
        i += 1
        
    plt.savefig("../Graphen/steering_calibration.png", figsize = (8, 6), dpi=160)
    plt.show()
    plt.close()


def plt_line(anfang, ende, linewidth):
    x1, y1 = anfang
    x2, y2 = ende
    
    plt.plot([x1, x2], [y1, y2], color = 'black', linewidth = linewidth)
 

    
def main():
    plotting(positions)
    
    
if __name__ == "__main__":
    plt_line((X_MIN, Y_MIN), (X_MAX, Y_MIN), linewidth=LINEWIDTH)
    plt_line((X_MAX, Y_MIN), (X_MAX, Y_MAX), linewidth=LINEWIDTH)
    plt_line((X_MAX, Y_MAX), (X_MIN, Y_MAX), linewidth=LINEWIDTH)
    plt_line((X_MIN, Y_MAX), (X_MIN, Y_MIN), linewidth=LINEWIDTH)    
    
    plt_line((LINKE_GRENZE, G1), (RECHTE_GRENZE, G1), linewidth=LINEWIDTH)   # G1
    plt_line((LINKE_GRENZE, G2), (RECHTE_GRENZE, G2), linewidth=LINEWIDTH)   # G2
    plt_line((LINKE_GRENZE, G13), (RECHTE_GRENZE, G13), linewidth=LINEWIDTH)       # G13
    plt_line((LINKE_GRENZE, G12), (RECHTE_GRENZE, G12), linewidth=LINEWIDTH)       # G12
    plt_line((LINKE_GRENZE, G23), (RECHTE_GRENZE, G23), linewidth=LINEWIDTH)       # G23
    plt_line((LINKE_GRENZE, G22), (RECHTE_GRENZE, G22), linewidth=LINEWIDTH)       # G22
    
    
    main()
    
