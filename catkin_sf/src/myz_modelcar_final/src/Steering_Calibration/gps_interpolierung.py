#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb  9 00:27:27 2019

@author: myz
"""
import redblackpy as rb
from matplotlib import pyplot as plt
import numpy as np

steering = [0, 30, 60, 90, 120, 150, 180]                           # entspricht dem Ausgaben
lenkung =  [-27.29, -25.20, -13.91, -7.806, 0.9522, 9.348, 17.554]    # entspricht den Eingaben
#angeln =  []
#for angel in lenkung:
#    angeln.append(angel*np.pi/180.0)
#

series = rb.Series(index = lenkung, values = steering2, dtype = 'float32', \
                   interpolate = 'linear')

#print(series[2.2])
#print(series[5])
#print(series)



Y = []
for x in X:
    if x < 0 or series[x] != 0:
        Y.append(series[x])
    else:
        Y.append(180)
    
plt.plot(X, Y)
plt.xlabel("Eigen. Lenkung")
plt.ylabel("Steering")
plt.title("Steering Calibration")
plt.savefig("../Graphen/steering_calibration", figsize=(8, 6), dpi=160)
plt.show()
plt.close()

