#! /usr/bin/env python
#! -*- coding: utf-8 -*-

from global_parameters import *
from global_functions import *
import rospy
from std_msgs.msg import Float64, String, Int16
from sensor_msgs.msg import LaserScan
from nav_msgs.msg import Odometry
import numpy as np
from matplotlib import pyplot as plt
import time


class Our_display:
    def __init__(self):
        self.code_x = np.nan
        self.code_y = np.nan
        self.car_alpha = np.nan
        self.timestamp = np.nan
        self.rpm = np.nan
        self.ms = np.nan
        self.tw_x = np.nan
        self.tw_y = np.nan
        self.tw_dir = np.nan
        self.point_cloud = []
        self.point_f = []
        self.point_t = []

        rospy.Subscriber("/sf_gps", String, self.gps_callback)
        rospy.Subscriber("/sf_speed_rpm", Float64, self.rpm_callback)
        rospy.Subscriber("/sf_speed_ms", Float64, self.ms_callback)
        rospy.Subscriber("/scan", LaserScan, self.lidar_callback)
        rospy.Subscriber("/sf_tw_x", Float64, self.tw_x_callback)
        rospy.Subscriber("/sf_tw_y", Float64, self.tw_y_callback)
        rospy.Subscriber("/sf_tw_dir", Float64, self.tw_dir_callback)
        rospy.Subscriber('sf_point_f', String, self.point_f_callback)
        rospy.Subscriber('sf_point_t', String, self.point_t_callback)
     

    def gps_callback(self, msg):
        self.code_x, self.code_y, self.car_alpha, self.timestamp = convert_str_to_list(msg.data)

    def rpm_callback(self, msg):
        self.rpm = msg.data

    def ms_callback(self, msg):
        self.ms = msg.data

    def tw_x_callback(self, msg):
        self.tw_x = msg.data

    def tw_y_callback(self, msg):
        self.tw_y = msg.data

    def tw_dir_callback(self, msg):
        self.tw_dir = msg.data

    def lidar_callback(self, msg):
        self.point_cloud = msg.ranges

    def point_f_callback(self, msg):
        self.point_f = convert_str_to_list(msg.data)

    def point_t_callback(self, msg):
        self.point_t = convert_str_to_list(msg.data)
        
        


def display_callback(display):
    # Die äußersten Linien
    plt.clf()


    plt_linie((X_MIN, Y_MIN), (X_MAX, Y_MIN))
    plt_linie((X_MAX, Y_MIN), (X_MAX, Y_MAX))
    plt_linie((X_MAX, Y_MAX), (X_MIN, Y_MAX))
    plt_linie((X_MIN, Y_MAX), (X_MIN, Y_MIN))
    
    plt_linie((LINKE_GRENZE, G1), (RECHTE_GRENZE, G1))
    plt_linie((LINKE_GRENZE, G2), (RECHTE_GRENZE, G2))
    plt_linie((LINKE_GRENZE, G13), (RECHTE_GRENZE, G13))
    plt_linie((LINKE_GRENZE, G12), (RECHTE_GRENZE, G12))
    plt_linie((LINKE_GRENZE, G23), (RECHTE_GRENZE, G23))
    plt_linie((LINKE_GRENZE, G22), (RECHTE_GRENZE, G22))


    code_x = display.code_x 
    code_y = display.code_y 
    tw_x = display.tw_x 
    tw_y = display.tw_y 
    plt.quiver([code_x], [code_y], \
               [tw_x - code_x], \
               [tw_y - code_y], \
               color = [AUTO_FARBE])
    
    plt_punkt(display.point_f, 'red')
    plt_punkt(display.point_t, 'green')

    plt.title("speed_rpm: {}; zeit: {}".format(display.rpm, time.ctime()))
    
    plt.pause(0.05)




#def plt_map():
#
#    X_MIN, X_MAX = 0, 6.00
#    Y_MIN, Y_MAX = 0, 4.30
##    Mittelpunkt_K1 = (1.95, 2.15)
##    Mittelpunkt_K2 = (4.05, 2.15)
#    G1 = 0.96               # Höhe von Gerade 1
#    G2 = 3.34              # Höhe von Gerade 2
#    G13 = 0.64
#    G12 = 0.32
#    G23 = 3.66
#    G22 = 3.98
#    LINEWIDTH = 0.01
#    LINKE_GRENZE, RECHTE_GRENZE = 1.95, 4.05
#    
#    
#    plt_line((X_MIN, Y_MIN), (X_MAX, Y_MIN), linewidth=LINEWIDTH)
#    plt_line((X_MAX, Y_MIN), (X_MAX, Y_MAX), linewidth=LINEWIDTH)
#    plt_line((X_MAX, Y_MAX), (X_MIN, Y_MAX), linewidth=LINEWIDTH)
#    plt_line((X_MIN, Y_MAX), (X_MIN, Y_MIN), linewidth=LINEWIDTH)    
#    
#    plt_line((LINKE_GRENZE, G1), (RECHTE_GRENZE, G1), linewidth=LINEWIDTH)   # G1
#    plt_line((LINKE_GRENZE, G2), (RECHTE_GRENZE, G2), linewidth=LINEWIDTH)   # G2
#    plt_line((LINKE_GRENZE, G13), (RECHTE_GRENZE, G13), linewidth=LINEWIDTH)       # G13
#    plt_line((LINKE_GRENZE, G12), (RECHTE_GRENZE, G12), linewidth=LINEWIDTH)       # G12
#    plt_line((LINKE_GRENZE, G23), (RECHTE_GRENZE, G23), linewidth=LINEWIDTH)       # G23
#    plt_line((LINKE_GRENZE, G22), (RECHTE_GRENZE, G22), linewidth=LINEWIDTH)       # G22
#    
    





def display_main(display):
    rospy.init_node('lidar_visual', anonymous=True)
    rate = rospy.Rate(10)
    while not rospy.is_shutdown():
        display_callback(display)
        rate.sleep()
            


if __name__ == '__main__':
    try:
        display = Our_display()
        display_main(display)
    except rospy.ROSInterruptException:
        pass
