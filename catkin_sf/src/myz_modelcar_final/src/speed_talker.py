#! /usr/bin/env python
# -*- coding: utf-8 -*-

import rospy
from global_functions import *
from geometry_msgs.msg import Twist
from std_msgs.msg import Float64


def speed_talker(msg):
    pub_rpm = rospy.Publisher('sf_speed_rpm', Float64, queue_size = QUEUE_SIZE)
    pub_ms = rospy.Publisher('sf_speed_ms', Float64, queue_size = QUEUE_SIZE)

    rpm = msg.linear.x
    ms = convert_rpm_to_ms(rpm)
    rospy.loginfo("rpm: {r}; ms: {m}".format(r=rpm, m=ms))

    pub_rpm.publish(rpm)
    pub_ms.publish(ms)




def speed_talker_main():
    rospy.init_node('speed_talker', anonymous=False)
    rospy.Subscriber('twist', Twist, speed_talker, queue_size = QUEUE_SIZE)
    rospy.spin()


if __name__ == '__main__':
    try:
        speed_talker_main()
    except rospy.ROSInterruptException:
        pass



 
