#! /usr/bin/env python
# -*- coding: utf-8 -*-

from global_functions import *
import numpy as np

AUTO_ID = 7         # der Alle wichtigster Parameter!!


RATE = 50
QUEUE_SIZE = 10


# ///////// Parameter für das Modelcar ////////
RAD_RADIUS = 0.03      # 3cm
OFFSET = 0.25          # 0.25m ist der Abstand zwischen dem Aruco-Code und dem Mittelpunkt der vorderen Axis
AUTO_LAENGE = 0.42

# ///////////// Für Display /////////////


X_MIN, X_MAX = 0, 6.00
Y_MIN, Y_MAX = 0, 4.30
Mittelpunkt_K1 = (1.95, 2.15)
Mittelpunkt_K2 = (4.05, 2.15)
G1 = 0.96               # Höhe von Gerade 1
G2 = 3.34              # Höhe von Gerade 2
G13 = 0.64
G12 = 0.32
G23 = 3.66
G22 = 3.98
#LINEWIDTH = 0.01
LINKE_GRENZE, RECHTE_GRENZE, MITTLE_GRENZE = 1.95, 4.05, 2.15
AUTO_FARBE = 'b'



# //////////////////// Klasse für Autos ////////////////////
class Auto(object):
    def __init__(self, id_num, rpm, lenkung):
        self.id_num = id_num
        self.id_odom = self.finde_id_odom()
        self.rpm = rpm
        self.lenkung = lenkung
        self.laenge = AUTO_LAENGE
        self.rad_radius = RAD_RADIUS
        self.offset = OFFSET
    
    def finde_id_odom(self):
        if self.id_num == 125:
            return 5
        elif self.id_num == 129:
            return 9
        elif self.id_num == 122:
            return 12
        elif self.id_num == 127:
            return 7
        elif self.id_num == 128:
            return 8
        elif self.id_num == 121:
            return 11
        elif self.id_num == 124:
            return 4
        elif self.id_num == 126:
            return 6
        elif self.id_num == 120:
            return 10
        elif self.id_num == 123:
            return 3
        return Exception





    
    
# initialisiere auto7
auto = Auto(127, np.nan, np.nan)


# //////////////////// Klasse für die Spuren /////////////////////

class Spur(object):
    def __init__(self, innere_untere_linie, auessere_untere_linie,  \
                 innere_obere_linie, auessere_obere_linie, radius, direction):
        """
        Mit Metrik Meter!
        """
        self.innere_untere_linie = innere_untere_linie
        self.auessere_untere_linie = auessere_untere_linie
        self.innere_obere_linie = innere_obere_linie
        self.auessere_obere_linie = auessere_obere_linie
        self.radius = radius
        self.direction = direction          # True entspricht der Richtung gegen den Uhrzeigersinn.
        self.mittlere_untere_linie = self.finde_mittlere_linie(True)
        self.mittlere_obere_linie = self.finde_mittlere_linie(False)
        self.linke_grenze_der_geraden = LINKE_GRENZE
        self.rechte_grenze_der_geraden = RECHTE_GRENZE
        self.mittlere_grenze = MITTLE_GRENZE
        self.mittelpunkt_link = Mittelpunkt_K1
        self.mittelpunkt_recht = Mittelpunkt_K2
        self.x_min = X_MIN
        self.x_max = X_MAX
        self.y_min = Y_MIN
        self.y_max = Y_MAX
        

    def finde_mittlere_linie(self, untere):
        if untere:
            ans = (self.innere_untere_linie + self.auessere_untere_linie) / 2.0
        else:
            ans = (self.innere_obere_linie + self.auessere_obere_linie) / 2.0
        return ans

    
    def beurteile_fahrrichtung(self, position, alpha):
        """
        Position entspricht (tw_x, tw_y), also die Position von dem Tourenwagen-Modelcar
        im Weltkoordinatensystem.
        alpha ist in sf_tw_dir von gps_talker gepublisht
        """
        viertel = feststelle_viertel(self, position)
        if self.direction:      # gegen den Uhrzeigersinn
            if viertel == 1:
                return False
            if viertel == 2:
                return (np.cos(alpha) > 0)
            if viertel == 4:
                return (np.cos(alpha) < 0)
            if viertel == 3:
                return (np.sin(alpha) > 0)
            if viertel == 5:
                return (np.sin(alpha) < 0)
            return True
        else:
            if viertel == 1:
                return False
            if viertel == 2:
                return (np.cos(alpha) < 0)
            if viertel == 4:
                return (np.cos(alpha) > 0)
            if viertel == 3:
                return (np.sin(alpha) < 0)
            if viertel == 5:
                return (np.sin(alpha) > 0)
            return True            


    def find_closest_point(self, position):
        x, y = position
        
        # Berechne den zutreffenden Punkt je nach den vier Bereichen
        if x < self.linke_grenze_der_geraden:
            ans = closest_point_to_a_halfcircle(position, self.mittelpunkt_link, self.radius)
            return ans
        elif x > self.rechte_grenze_der_geraden:
            ans = closest_point_to_a_halfcircle(position, self.mittelpunkt_recht, self.radius)
            return ans
        elif y < self.mittlere_grenze:
            ans = closest_point_to_a_line(position, self.mittlere_untere_linie)
            return ans
        else:
            ans = closest_point_to_a_line(position, self.mittlere_obere_linie)
            
        return ans


# ////////////////////// initialisiere spur1 & spur2 ///////////////////////////////

radius_s1 = (G23 - G1) / 2.0
radius_s2 = (G22 - G13) / 2.0 
spur1 = Spur(G1, G13, G2, G23, radius_s1, True)
spur2 = Spur(G13, G12, G23, G22, radius_s2, True)








# ////////////// redudante /////////////////
HERZ = 50
QUEUESIZE = 10
STOP = False
STOP_SPEED = 0
STOP_STEERING = 90
DRIVER_NODE = "/driver_node"
