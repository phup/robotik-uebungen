#! /usr/bin/env python
# -*- coding: utf-8 -*-

from global_parameters import *
import numpy as np
from matplotlib import pyplot as plt




# /////////////////// für Einheitenkonvertierungen //////////////////

def convert_str_to_list(s):
    s = s[1 : -1]
    ans = []
    s_liste = s.split(',')
    for item in s_liste:
        ans.append(float(item))

    return ans


def convert_rpm_to_ms(rpm):
    ans = (2 * np.pi * RAD_RADIUS * rpm) / 60.0
    return ans



# /////////////// für Kalibrierungen ///////////////////

def speed_calibration(auto):
    """
    Diese Calibration betrifft das Modelcar 127: steve.

    speed_calibration macht nur dann Sinn, wenn das rpm unter etwa
    260 liegt. Wenn es über 260 ist, verhältet sich dann zunächst
    sprunghaft nach vorne. Und 1 Sekunde danach fährt das Auto sowieso
    wieder zurück auf circa 260.

    Wir nehmen an, dass es so mit Absicht festgesetzt wurde.
    """
    if auto.id_odom != 7:
        return auto.rpm

    speed_nxt = 2.4 * auto.rpm + 76
    return speed_nxt     # Die wird abschließend zum Topic /manual_control/speed gepublisht.


def steering_calibration(auto):
    """
    Ebenfalls betrifft der Code vorläufig nur das Modelcar 127: steve.

    Dabei ist es uns aufgefallen, dass der Steering gar nicht mal so
    flink, wie man sich vorstellt. Also, der Steering verändert die
    Lenkung offenbar eh stufenhaft als irgendwie in einer kontinuierlichen
    Weise.

    Dabei entspricht eine 10-große Veränderung bei dem Steering ziemlich genau
    einer 3-Grad-Drehung des Vorderrads. Vermutlich verhältet sich es generall
    so bei jedem Auto.

    Das wird wiederum bedeuten, dass der ideale Drehwinkel [-27 Grad, 27 Grad]
    sein sollte. Bei den meisten Modelcars sind es jedoch verschoben. 

    Konkret verschiebt sich der Drehwinkel von steve ohne Kalibrierung
    stark, circa. 12 Grad, nach links.
    """
    if auto.id_odom != 7:
        return auto.lenkung

    if auto.lenkung >= convert_grad_to_angel(19.5):
        return 180      # praktisch steering_nxt, das zum Topic /steering gepublisht wird
    elif auto.lenkung >= convert_grad_to_angel(16.5):
        return 170
    elif auto.lenkung >= convert_grad_to_angel(13.5):
        return 160
    elif auto.lenkung >= convert_grad_to_angel(10.5):
        return 150
    elif auto.lenkung >= convert_grad_to_angel(7.5):
        return 140
    elif auto.lenkung >= convert_grad_to_angel(4.5):
        return 130
    elif auto.lenkung >= convert_grad_to_angel(1.5):
        return 120
    elif auto.lenkung >= convert_grad_to_angel(-1.5):
        return 110
    elif auto.lenkung >= convert_grad_to_angel(-4.5):
        return 100
    elif auto.lenkung >= convert_grad_to_angel(-7.5):
        return 90
    elif auto.lenkung >= convert_grad_to_angel(-10.5):
        return 80
    elif auto.lenkung >= convert_grad_to_angel(-13.5):
        return 70
    elif auto.lenkung >= convert_grad_to_angel(-16.5):
        return 60
    elif auto.lenkung >= convert_grad_to_angel(-19.5):
        return 50
    else:
        return 40
    
    """
    Wir versuchen dabei, dass das Auto sich einigermaßen beim Lenken symmatisch
    verhältet, weshalb wir das Steering unter 40 nicht berücksichtigt.
    """


def convert_grad_to_angel(grad):
    ans = np.pi*grad/180.0
    return ans



# /////////////////////// für die Klasse Spur ///////////////////////////

def closest_point_to_a_line(position, h):
    """
    Die gegebene Gerade: y = h
    """
    x, y = position
    
    return (x, h)


def closest_point_to_a_halfcircle(punkt, mittelpunkt, r):
    """
    (M_x, M_y) ist der Mittelpunkt des Kreises, und r ist der Radius
    """
    x, y = punkt
    M_x, M_y = mittelpunkt
    
    d = np.sqrt((x - M_x)**2 + (y - M_y)**2)
    P_x = r*(float(x - M_x)/d) + M_x
    P_y = r*(float(y - M_y)/d) + M_y
    
    return (P_x, P_y)


def feststelle_viertel(spur, position):
    """
    1. außerhalb der gesamten viereckigen Strecke
    2. Viertel, in dem das Auto insgesamt zum Weltkoordinatensystem nach recht fahren muss
    (natürlich vorausgesetzt, dass das Auto gegen den Uhrzeigersinn fährt. Ansonsten genau umgekehrt)
    3. Viertel, in dem das Auto insgesamt nach oben fahren muss
    4. Viertel, in dem das Auto insgesamt nach links fahren muss
    5. Viertel, in dem das Auto insgesamt nach unten fahren muss
    6. Viertel, der sich in der Mitte. Keine Vorschrift.
    """
    x, y = position
    
    if x < spur.x_min or x > spur.x_max:
        return 1
    if y < spur.y_min or y > spur.y_max:
        return 1
    
    if y <= spur.innere_untere_linie:
        return 2
    
    if y >= spur.innere_obere_linie:
        return 4
    
    if x >= spur.rechte_grenze_der_geraden:
        return 3
    
    if x <= spur.linke_grenze_der_geraden:
        return 5
    
    return 6



# //////////////////// für die Klasse Auto ////////////////////





# ///////////////////// für Display /////////////////////
    




def plt_linie(anfang, ende, linewidth = 1):
    x1, y1 = anfang
    x2, y2 = ende
    
    plt.plot([x1, x2], [y1, y2], color = 'black', linewidth = linewidth)

def plt_punkt(punkt, color):
    x, y = punkt
    plt.scatter([x], [y], color=color)







#if __name__ == "__main__":
 #   ans = convert_str_to_list('[1.234, 4.321, 234.32, 0.2201]')
# #   print(ans)
#    ans = convert_rpm_to_ms(85)
#    print (ans)
