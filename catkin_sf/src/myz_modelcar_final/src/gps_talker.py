#! /usr/bin/env python
# -*- coding: utf-8 -*-

from global_parameters import *
import rospy
from std_msgs.msg import Float64, String
from nav_msgs.msg import Odometry
import numpy as np


def gps_talker(msg):
    pub = rospy.Publisher('sf_gps', String, queue_size = QUEUE_SIZE)
    pub_tw_x = rospy.Publisher('sf_tw_x', Float64, queue_size = QUEUE_SIZE)
    pub_tw_y = rospy.Publisher('sf_tw_y', Float64, queue_size = QUEUE_SIZE)
    pub_tw_dir = rospy.Publisher('sf_tw_dir', Float64, queue_size = QUEUE_SIZE)
   
    x = msg.pose.pose.position.x
    y = msg.pose.pose.position.y
    z = msg.pose.pose.orientation.z                 # entspricht 2*arcsin(alpha)
    alpha = 2 * np.arcsin(z)                        # alpha entspricht der Fahrrichtung
    tw_x, tw_y = berechne_tw_position(x, y, alpha)
    tw_dir = alpha                                  # alpha entspricht einfach der Fahrrichtung
  #  seq = msg.header.seq
    secs = msg.header.stamp.secs
    nsecs = msg.header.stamp.nsecs
    timestamp = secs % 10000 + nsecs * (10**(-9))        # was uns interesiert, ist nur unter Sekunde
    
    nxt_msg = str((x, y, alpha, timestamp))
    rospy.loginfo(nxt_msg)
    pub.publish(nxt_msg)

    tw_nachricht = "\n***** tw_x: {}; tw_y: {}; tw_dir: {}*****\n".format(tw_x, tw_y, tw_dir)
    rospy.loginfo(tw_nachricht)
    pub_tw_x.publish(tw_x)
    pub_tw_y.publish(tw_y)
    pub_tw_dir.publish(tw_dir)


def berechne_tw_position(x, y, alpha, offset = OFFSET):
    tw_x = x + offset * np.cos(alpha)
    tw_y = y + offset * np.sin(alpha)
    return tw_x, tw_y




def gps_talker_main():
    rospy.init_node('gps_talker', anonymous=False)
    topics = "localization/odom/" + str(AUTO_ID)      # z.B. AUTO_ID = 7 für Auto 127
    rospy.Subscriber(topics, Odometry, gps_talker, queue_size = QUEUE_SIZE)
    rospy.spin()

if __name__ == '__main__':
    try:
        gps_talker_main()
    except rospy.ROSInterruptException:
        pass




