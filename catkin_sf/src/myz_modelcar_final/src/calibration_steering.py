#! /usr/bin/env python

from global_parameter import *
import rospy
from std_msgs.msg import LaserScan



def lidar_callback(msg):
    print (type(msg.ranges))     # test


def lidar_visualizieren():
    rospy.init_node('lidar_visualization', anonymous = False)
    rospy.Subscriber("/scan", LaserScan, lidar_callback)

    rospy.spin()


if __name__ == "__main__":
    lidar_visualizieren()
    
