#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Feb  8 11:11:14 2019

@author: myz
"""

from matplotlib import pyplot as plt



X_MIN, X_MAX = 0, 600
Y_MIN, Y_MAX = 0, 430
Mittelpunkt_K1 = (195, 215)
Mittelpunkt_K2 = (405, 215)
G1 = 96               # Höhe von Gerade 1
G2 = 334              # Höhe von Gerade 2
G13 = 64
G12 = 32
G23 = 366
G22 = 398



LINEWIDTH = 1

LINKE_GRENZE, RECHTE_GRENZE, MITTLE_GRENZE = 195, 405, 215


def plt_line(anfang, ende, linewidth):
    x1, y1 = anfang
    x2, y2 = ende
    
    plt.plot([x1, x2], [y1, y2], color = 'black', linewidth = linewidth)

#plt.plot(x_liste, y_liste, 'black')
    
    
    
    
def main():
    # Die äußersten Linien
    plt_line((X_MIN, Y_MIN), (X_MAX, Y_MIN), linewidth=LINEWIDTH)
    plt_line((X_MAX, Y_MIN), (X_MAX, Y_MAX), linewidth=LINEWIDTH)
    plt_line((X_MAX, Y_MAX), (X_MIN, Y_MAX), linewidth=LINEWIDTH)
    plt_line((X_MIN, Y_MAX), (X_MIN, Y_MIN), linewidth=LINEWIDTH)    
    
    plt_line((LINKE_GRENZE, G1), (RECHTE_GRENZE, G1), linewidth=LINEWIDTH)   # G1
    plt_line((LINKE_GRENZE, G2), (RECHTE_GRENZE, G2), linewidth=LINEWIDTH)   # G2
    plt_line((LINKE_GRENZE, G13), (RECHTE_GRENZE, G13), linewidth=LINEWIDTH)       # G13
    plt_line((LINKE_GRENZE, G12), (RECHTE_GRENZE, G12), linewidth=LINEWIDTH)       # G12
    plt_line((LINKE_GRENZE, G23), (RECHTE_GRENZE, G23), linewidth=LINEWIDTH)       # G23
    plt_line((LINKE_GRENZE, G22), (RECHTE_GRENZE, G22), linewidth=LINEWIDTH)       # G22

   
#    circle = plt.Circle((50, 50), 30)
#    ax = plt.gca()
#    ax.add_artist(circle)

     
        
if __name__ == "__main__":
    main()
    code_x, code_y = 100, 200
    tw_x, tw_y = 50, 100
    plt.quiver([code_x], [code_y], [tw_x - code_x], [tw_y - code_y], color=['b', 'r'])
    plt.show()
    
